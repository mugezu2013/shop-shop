const path = require('path');
const BundleTracker = require('webpack-bundle-tracker');

const outputDir = '../server/apps/frontend/static/frontend';

module.exports = {
  lintOnSave: true,

  css: {
    loaderOptions: {
      postcss: {
        config: {
          // Fixed postcss configuration location inside monorepos
          path: path.join(__dirname, 'postcss.config.js'),
        },
      },
    },
  },

  runtimeCompiler: true,

  chainWebpack: config => {
    /**
     * alias example
    * */
    // config.resolve.alias
    //   .set('@md', path.resolve(__dirname, 'src/modules/'));
    

    config.module.rule('pug')
      .oneOf('pug-vue')
      .use('pug-plain-loader')
        .loader('pug-plain-loader')
        .tap(options => ({
          ...options,
          basedir: path.resolve(__dirname, 'src/templates'),
        }));

    config.module.rule('svg')
      .test(/(?<!\.i)(\.(svg)(\?.*)?)$/);

    config.module.rule('svg-icons')
      .test(/(?<=\.i)(\.(svg)(\?.*)?)$/)
      .use('babel-loader')
        .loader('babel-loader')
        .end()
      .use('vue-svg-loader')
        .loader('vue-svg-loader')
        .options({
          svgo: {
            plugins: [
              { prefixIds: true },
            ],
          },
        });
  },

  publicPath: (process.env.NODE_ENV === 'production' || process.env.FRONTEND_WATCH)
    ? process.env.FRONTEND_LOCATION_PATH
    : '/',
  outputDir,

  devServer: {
    public: process.env.FRONTEND_LOCATION_BASE,
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Origin': process.env.FRONTEND_ORIGIN_URL
        ? process.env.FRONTEND_ORIGIN_URL
        : '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, content-type, Authorization',
    },
  },

  configureWebpack: {
    plugins: [
      new BundleTracker({
        publicPath: process.env.NODE_ENV === 'production'
          ? process.env.FRONTEND_LOCATION_PATH
          : process.env.FRONTEND_LOCATION_URL.replace(/\/+$/img, '') + process.env.FRONTEND_LOCATION_PATH,
        filename: `${outputDir}/webpack-stats.json`,
      }),
    ],
  },
};
