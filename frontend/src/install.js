import VueI18n from 'vue-i18n'

export function install(Vue) {
  Vue.use(VueI18n)
}

export default { install }
