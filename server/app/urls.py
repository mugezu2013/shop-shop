from des import urls as des_urls
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import static
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^admin/', admin.site.urls),
    url(r'^django-des/', include(des_urls)),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('robots.txt', include('robots.urls')),

    # url(r'', include('apps.frontend.urls')),
]

api_urlpatterns = [
    re_path(r'api/v(?P<version>[1|2]+)/', include([
        path('', include('apps.novaposhta.urls')),
        path('', include(('apps.accounts.api.urls'))),
    ])),
    # url(r'^rest-auth/', include('rest_auth.urls'))

]
urlpatterns += api_urlpatterns

schema_view = get_schema_view(
    openapi.Info(
        title="API",
        default_version='v1',
        description="Test description",
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
    patterns=api_urlpatterns
)
urlpatterns += [
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0),
         name='schema-swagger-ui'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
]
urlpatterns += i18n_patterns(
    path('', include('apps.addition_page.urls')),
    path('', include('apps.core.urls', '')),
    path('accounts/social/',
         include(('apps.social_accounts.urls', 'social_account'))),
    path('accounts/', include(('apps.accounts.urls', 'account'))),
    path('', include(('apps.store.urls', 'store'))),
    path('accounts/', include('allauth.urls')),

    prefix_default_language=False
)

handler404 = 'apps.core.views.page_404'
handler403 = 'apps.core.views.page_403'
if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [url(r'^rosetta/', include('rosetta.urls'))]

if settings.DEBUG:
    urlpatterns += [url(r'^', include('markup.urls'))]
    urlpatterns += (
            static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) +
            static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    )

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
