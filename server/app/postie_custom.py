from postie.shortcuts import send_mail


def send_mail_custom(**kwargs):
    from des.models import DynamicEmailConfiguration
    des_config = DynamicEmailConfiguration.get_solo()
    send_mail(
        from_email=des_config.host,
        **kwargs
    )
