import os

from celery import Celery
# Задаем переменную окружения, содержащую название файла настроек нашего проекта.
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')

app = Celery('app')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
app.conf.timezone = 'UTC'

app.conf.beat_schedule = {
    'get_actual_curse_money': {
        'task': 'apps.core.tasks.get_actual_curse_money',
        'schedule': crontab(minute=0, hour="*/1")
    },
    'get_novaposhta_data': {
        'task': 'apps.novaposhta.tasks.sync_data',
        'schedule': crontab(minute=36, hour="*/24")
    },
}
