from django.utils.translation import pgettext_lazy
from django.utils.translation import ugettext_lazy as _

# Postie
from model_utils import Choices

__all__ = (
    'POSTIE_HTML_ADMIN_WIDGET',
    'POSTIE_INSTANT_SEND',
    'ADMIN_POSTIE_CHOICES',
    'POSTIE_TEMPLATE_CHOICES',
    'POSTIE_TEMPLATE_CONTEXTS'
)

POSTIE_HTML_ADMIN_WIDGET = {
    'widget': 'CKEditorUploadingWidget',
    'widget_module': 'ckeditor_uploader.widgets',
    'attrs': {},
}
ADMIN_POSTIE_CHOICES = Choices(
    ('new_order_for_recipients', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Новый заказ для получателей)',
        'Новый заказ для получателей'
    )),
    ('new_user_for_recipients', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Новый пользователь для получателей)',
        'Новый пользователь для получателей'
    )),
    ('new_review', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Новый отзыв на товар)',
        'Новый отзыв на товар'
    )),
    ('new_feedback', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Новый отзыв)',
        'Новый отзыв'
    )),
)

POSTIE_TEMPLATE_CHOICES = Choices(
    ('сonfirm_email', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Подтвердите почту)',
        'Подтвердите почту'
    )),
    ('сonfirm_email_with_password', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Подтвердите почту с паролем)',
        'Подтвердите почту с паролем'
    )),
    ('social_registration', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Социальная регистрация)',
        'Социальная регистрация'
    )),
    ('password_reset', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Восстановить пароль)',
        'Восстановить пароль'
    )),
    ('new_order', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Новый заказ)',
        'Новый заказ'
    )),
    ('feedback_retry', pgettext_lazy(
        'default.POSTIE_TEMPLATE_CHOICES(Ответ на отзыв)',
        'Ответ на отзыв'
    )),

) + ADMIN_POSTIE_CHOICES

POSTIE_TEMPLATE_CONTEXTS = {
    'сonfirm_email': {
        'email': _('Почта'),
        'activation_link': _('Ссылка для подтверждения'),
    },
    'сonfirm_email_with_password': {
        'email': _('Логин'),
        'activation_link': _('Ссылка для подтверждения'),
        'password': _('Ваш пароль, который рекомендуем заменить')
    },
    'social_registration': {
        'email': _('Логин'),
        'password': _('Ваш пароль, который рекомендуем заменить')
    },
    'password_reset': {
        'password_reset_link': _('Ссылка для востановления'),
        'user_email': _('Почта'),
    },
    'new_review': {
        'link_review': _('Ссылка на отзыв'),
    },
    'new_order': {
        'first_name': _('Имя'),
        'number_order': _('Номер заказа'),
    },
    'feedback_retry': {
        'massage': _('Вопрос'),
        'admin_massage': _('Ответ'),
    },

    'new_order_for_recipients': {
        'link': _('Ссылка на заказ'),
    },
    'new_user_for_recipients': {
        'link': _('Ссылка на пользователя'),
    },
    'new_feedback': {
        'link': _('Ссылка на отзыв'),
    },

}

POSTIE_INSTANT_SEND = True
