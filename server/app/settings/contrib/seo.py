from ..default import env, MIDDLEWARE

SEO_USE_URL_SEO = True
SEO_DEBUG_MODE = env.bool("SEO_DEBUG_MODE", True)

MIDDLEWARE += [
    'seo.middleware.url_seo_middleware'
]
SEO_HTML_ADMIN_WIDGET = {
    'widget': 'CKEditorWidget',
    'widget_path': 'ckeditor.widgets',
}
SEO_MODELS = [

    'store.Product',
    # 'store.Brand',
    # 'store.ProductCategory',
    # 'pages.Page',
    # 'sales.Campaign',
    # 'event.Event',
    # 'blog.Post',
    # 'blog.VideoPost',
    # 'knowledge_base.Category',
]
