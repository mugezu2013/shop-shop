from decimal import Decimal

from constance import config

__all__ = (
    'CONSTANCE_CONFIG',
    'CONSTANCE_CONFIG_FIELDSETS',
    'CONSTANCE_BACKEND',
    'CONSTANCE_ADDITIONAL_FIELDS',
)

CONSTANCE_ADDITIONAL_FIELDS = {
    # 'widget-name': [
    #     'django.forms.CharField',
    #     {
    #        'queryset': lazy(get_groups_queryset, models.QuerySet)(),
    #        'required': True,
    #        'widget': 'django.forms.Textarea',
    #        'widget_kwargs': {'attrs':{'cols': '60', 'rows': '5'}},
    #     }
    # ],
    'decimal-field': [
        'django.forms.DecimalField',
        {}
    ],
    'image-field': [
        'django.forms.FileField',
        {}
    ],
    'key-field': [
        'django.forms.CharField',
        {

        }
    ],
}

CONSTANCE_CONFIG = {
    # 'CONSTANCE': (
    #     "Default value", "Help text", 'widget',
    # ),
    # 'PAYPAL_CLIENT_ID': (
    #     '', 'https://developer.paypal.com/developer/applications'
    # ),
    # 'PAYPAL_SECRET': (
    #     '', 'https://developer.paypal.com/developer/applications'
    # ),
    'REFERRAL_NEW_USER_REWARD': (
        Decimal('10.0'), '', 'decimal-field'
    ),
    'DEFAULT_IMAGE_404':
        ('default/404_image.jpg', 'image', 'image-field'),
    'TINYPNG_API_KEY': (
        '3Y4rfMDtwYmglj8zJdFQ67fYSPXFwtkc', 'char', 'key-field'
    ),
    'LIQPAY_PUBLIC_KEY': (
        'sandbox_i87869137150', 'char', 'key-field'
    ),
    'LIQPAY_PRIVATE_KEY': (
        'sandbox_m3XlyszNhPWG5QDtvTgtPhHacD8STzcSvdJ9pSHy', 'char', 'key-field'
    ),

}

CONSTANCE_CONFIG_FIELDSETS = {
    # 'Fieldset': (
    #     'CONSTANCE',
    # ),
    # 'Fieldset2': {
    #     'fields': (
    #         'CONSTANCE',
    #     ),
    #     'collapse': True,
    # },
    # 'INTEGRATIONS': (
    #     'PAYPAL_CLIENT_ID',
    #     'PAYPAL_SECRET',
    # ),
    'SYSTEM SETTINGS': (
        'REFERRAL_NEW_USER_REWARD',
        'DEFAULT_IMAGE_404',
        'TINYPNG_API_KEY',
    ),

    'PAY': (
        'LIQPAY_PUBLIC_KEY',
        'LIQPAY_PRIVATE_KEY',
    ),

}

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'


def get_tiny_api_key():
    return config.TINYPNG_API_KEY
