from django.urls import path, re_path

from apps.social_accounts.views import CustomSocialSignupView

urlpatterns = [
    # path(
    #     "login/cancelled/",
    #     views.login_cancelled,
    #     name="socialaccount_login_cancelled",
    # ),
    # path("login/error/", views.login_error, name="socialaccount_login_error"),
    path("signup/", CustomSocialSignupView.as_view(), name="socialaccount_signup"),
    # path("connections/", views.connections, name="socialaccount_connections"),
]
