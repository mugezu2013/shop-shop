# from django.contrib.sites.models import Site
# from django.template import loader
#
# from app.celery import app
# from app.postie_custom import send_mail_custom
# from apps.accounts.selectors import ADMIN_OR_STAFF_USER
#
#
# @app.task
# def email_confirm_mail_task(code, email, password=None):
#     domain = Site.objects.get_current().domain
#     context = {
#         'code': code,
#         'email': email,
#         'domain': domain,
#         'protocol': 'http://'
#     }
#     url_success = loader.render_to_string(
#         'accounts/email_success.jinja',
#         context
#     )
#     if password:
#         send_mail_custom(
#             event='сonfirm_email_with_password',
#             recipients=[email, ],
#             context={
#                 'email': email,
#                 'activation_link': url_success,
#                 'password': password
#             },
#         )
#     else:
#         send_mail_custom(
#             event='сonfirm_email',
#             recipients=[email, ],
#             context={
#                 'email': email,
#                 'activation_link': url_success,
#             },
#         )
#
#
# @app.task
# def email_reset_mail_task(to_email, body):
#     send_mail_custom(
#         event='password_reset',
#         recipients=[to_email, ],
#         context={
#             'password_reset_link': body,
#             'user_email': to_email,
#         },
#     )
#
#
# @app.task
# def social_registration(email, password):
#     send_mail_custom(
#         event='social_registration',
#         recipients=[email, ],
#         context={
#             'email': email,
#             'password': password
#         },
#     )
#
#
# @app.task
# def new_user_for_recipients(url_user):
#     email_list_admin = list([obj.email for obj in ADMIN_OR_STAFF_USER])
#
#     send_mail_custom(
#         event='new_user_for_recipients',
#         recipients=email_list_admin,
#         context={
#             'link': url_user,
#         },
#     )
