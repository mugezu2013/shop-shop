from allauth.socialaccount.views import SignupView


class CustomSocialSignupView(SignupView):
    template_name = 'social_account/singup.jinja'
