from allauth.account.adapter import get_adapter as get_account_adapter
from allauth.account.utils import user_email
from allauth.socialaccount import app_settings
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.utils import email_address_exists
from django.contrib.auth.models import User
from django.urls import reverse

from apps.accounts.tasks import social_registration, new_user_for_recipients
from shared.utils.base import string_generator


class CustomSocialAccountAdapter(DefaultSocialAccountAdapter):

    def save_user(self, request, sociallogin, form=None):
        print('SSSSSSSocial save ' * 5)
        email = user_email(sociallogin.user)
        user = User.objects.filter(email=email).first()
        u = sociallogin.user

        if user:
            user.is_active = True
            sociallogin.connect(request, user)
        else:
            if form:
                get_account_adapter().save_user(request, u, form)
            else:
                get_account_adapter().populate_username(request, u)
            password = string_generator()
            u.set_password(password)
            sociallogin.save(request)
            email = user_email(sociallogin.user)
            social_registration.delay(email, password)
            link_user_admin = request.build_absolute_uri(
                reverse('admin:auth_user_change', args=(u.id,)))
            new_user_for_recipients.delay(link_user_admin)
        return u

    def is_auto_signup_allowed(self, request, sociallogin):
        auto_signup = app_settings.AUTO_SIGNUP
        if auto_signup:
            email = user_email(sociallogin.user)
            # Let's check if auto_signup is really possible...
            if email:
                if email_address_exists(email):
                    auto_signup = True

            elif app_settings.EMAIL_REQUIRED:
                # Nope, email is required and we don't have it yet...
                auto_signup = False
        return auto_signup
