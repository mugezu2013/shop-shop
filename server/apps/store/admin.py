from adminsortable2.admin import SortableInlineAdminMixin, SortableAdminMixin
from django import forms
from django.contrib import admin
from modeltranslation.admin import (TabbedDjangoJqueryTranslationAdmin,
                                    TranslationStackedInline)
from mptt.admin import DraggableMPTTAdmin
from seo.admin import ModelInstanceSeoInline

from .models import (
    Category, Image, Brand, Product, ProductVariant, Size, Color, Review,
    MethodPayment, Basket, Order, OrderProduct, Pay, AddressDelivery, Payment,
    BasketProduct, MethodDelivery
)


class ImageStackedInline(SortableInlineAdminMixin, TranslationStackedInline):
    model = Image
    extra = 0


class ProductVariantStackedInline(SortableInlineAdminMixin,
                                  TranslationStackedInline):
    model = ProductVariant
    extra = 1


class BasketProductStackedInline(admin.StackedInline):
    model = BasketProduct
    extra = 1


class OrderProductStackedInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OrderProductStackedInlineForm, self).__init__(*args, **kwargs)
        self.fields['product_variant'].queryset = (
            ProductVariant.objects.all()
                .select_related('size', 'color', 'product')
        )

    class Meta:
        model = OrderProduct
        exclude = ('',)


class OrderProductStackedInline(admin.StackedInline):
    model = OrderProduct
    extra = 1
    form = OrderProductStackedInlineForm
    # def get_queryset(self, request):
    #     return super().get_queryset(request).prefetch_related(
    #         'product_variant',
    #     )


class PayStackedInline(admin.StackedInline):
    model = Pay
    extra = 1


class AddressDeliveryStackedInline(admin.StackedInline):
    model = AddressDelivery
    extra = 1


class PaymentStackedInline(TranslationStackedInline):
    model = Payment
    extra = 1


@admin.register(Category)
class CategoryAdmin(DraggableMPTTAdmin, TabbedDjangoJqueryTranslationAdmin):
    list_display = (
        'tree_actions', 'indented_title', 'name', 'slug', 'is_active',
        'description')
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name',)
    list_display_links = ('indented_title',)


@admin.register(Product)
class ProductAdmin(SortableAdminMixin, TabbedDjangoJqueryTranslationAdmin):
    list_display = ('title', 'news', 'top_buy', 'top_product', 'created_at')
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title', 'news', 'top_buy', 'top_product')
    readonly_fields = ('created_at', 'updated_at')
    inlines = (ProductVariantStackedInline, ModelInstanceSeoInline)


@admin.register(ProductVariant)
class ProductVariantAdmin(SortableAdminMixin,
                          TabbedDjangoJqueryTranslationAdmin):
    list_display = (
        'product', 'vendor_code', 'price', 'price_sale', 'size', 'color',
        'is_active',)
    search_fields = ('vendor_code',)
    list_filter = ('size', 'color', 'is_active')
    readonly_fields = ('created_at', 'updated_at')
    inlines = (ImageStackedInline,)


@admin.register(Size)
class SizeAdmin(SortableAdminMixin, TabbedDjangoJqueryTranslationAdmin):
    list_display = ('title', 'slug', 'description',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)


@admin.register(Color)
class ColorAdmin(SortableAdminMixin, TabbedDjangoJqueryTranslationAdmin):
    list_display = ('title', 'slug', 'description',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)


@admin.register(Brand)
class BrandAdmin(SortableAdminMixin, TabbedDjangoJqueryTranslationAdmin):
    list_display = ('title', 'slug', 'description',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = (
        'product', 'user', 'status', 'rating', 'comment', 'email', 'user_name',
        'created_at', 'updated_at')
    list_filter = ('user', 'status',)
    readonly_fields = ('created_at', 'updated_at')

    # list_editable = ('status',)

    def get_queryset(self, request):
        return Review.objects.all().prefetch_related('user')


# -----------------------------------------------------------------------------

@admin.register(Basket)
class BasketAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ('user', 'status', 'session_key', 'count', 'price',
                    'created_at', 'updated_at')
    list_filter = ('user', 'status',)
    readonly_fields = ('created_at', 'updated_at', 'session_key',)
    inlines = (BasketProductStackedInline,)


@admin.register(MethodPayment)
class MethodPaymentAdmin(SortableAdminMixin,
                         TabbedDjangoJqueryTranslationAdmin):
    list_display = ('title', 'description', 'is_active')


@admin.register(MethodDelivery)
class MethodDeliveryAdmin(SortableAdminMixin,
                          TabbedDjangoJqueryTranslationAdmin):
    list_display = ('title', 'description', 'price_delivery', 'is_active')


@admin.register(Order)
class OrderAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = (
        'number_order', 'user', 'status', 'basket', 'delivery',
        'payment_method', 'payment_status',
        'created_at', 'updated_at')
    list_filter = ('user', 'status', 'delivery',
                   'payment_method', 'payment_status')
    readonly_fields = ('created_at', 'updated_at', 'number_order')
    inlines = (
        OrderProductStackedInline,
        PayStackedInline, AddressDeliveryStackedInline, PaymentStackedInline)
