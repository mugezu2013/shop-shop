from string import Template

from django import forms
from django.core.validators import validate_image_file_extension
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from apps.novaposhta.models import Settlement, Warehouse
from .models import Review, BasketProduct, Order


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ('email', 'user_name', 'rating', 'comment')


class BasketProductForm(forms.ModelForm):
    in_basket = forms.BooleanField(widget=forms.HiddenInput(),
                                   required=False)

    class Meta:
        model = BasketProduct
        fields = ('count',)


class PictureWidget(forms.widgets.Widget):
    def render(self, name, value, attrs=None, renderer=None):
        html = Template(
            """<img readonly='readonly' disabled=True class="img-thumbnail" 
            width="150" height="150" src="$link"/>""")
        return mark_safe(html.substitute(link=value))


class BasketVariantProductForm(forms.ModelForm):
    image = forms.FileField(
        label=_('Превью'),
        required=False,
        validators=[validate_image_file_extension],
        widget=PictureWidget,
    )
    product_variant = forms.CharField(max_length=254, label=_('Продукт'),
                                      required=False,
                                      widget=forms.TextInput(
                                          attrs={'readonly': 'readonly',
                                                 'disabled': True,
                                                 }
                                      ))
    color = forms.CharField(max_length=254, label=_('Цвет'),
                            required=False,
                            widget=forms.TextInput(
                                attrs={'readonly': 'readonly',
                                       'disabled': True, }
                            ))
    size = forms.CharField(max_length=254, label=_('Размер'),
                           required=False,
                           widget=forms.TextInput(
                               attrs={'readonly': 'readonly',
                                      'disabled': True, }
                           ))
    price = forms.IntegerField(label=_('Цена за единицу'),
                               required=False,
                               widget=forms.TextInput(
                                   attrs={'readonly': 'readonly',
                                          'disabled': True, }
                               ))
    price_total = forms.IntegerField(label=_('Цена'),
                                     required=False,
                                     widget=forms.TextInput(
                                         attrs={'readonly': 'readonly',
                                                'disabled': True, }
                                     ))

    def __init__(self, *args, **kwargs):
        super(BasketVariantProductForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance', None)
        if instance:
            self.initial['product_variant'] = instance.product_variant.product
            self.initial['color'] = instance.product_variant.color
            self.initial['size'] = instance.product_variant.size
            self.initial['price'] = instance.product_variant.price
            self.initial['image'] = instance.product_variant.product.preview.url
            self.initial['price_total'] = \
                instance.product_variant.price * instance.count

    class Meta:
        model = BasketProduct
        fields = ('count',)


class OrderingForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = (
            # 'email', 'first_name', 'last_name', 'create', 'user',
            'payment_method', 'delivery',
        )

    def __init__(self, *args, **kwargs):
        super(OrderingForm, self).__init__(*args, **kwargs)
        self.fields['payment_method'].required = True
        self.fields['delivery'].required = True


class NovaPoshtaForm(forms.Form):
    search_city = forms.CharField(label=_('Город'))
    city = forms.ModelChoiceField(label=_(' '),
                                  required=True,
                                  queryset=Settlement.objects.none(),
                                  widget=forms.Select)
    street = forms.ModelChoiceField(label=_('Отделение'),
                                    required=True,
                                    queryset=Warehouse.objects.none(),
                                    widget=forms.Select)

    class Meta:
        fields = ('search_city', 'city', 'street')

    def __init__(self, *args, **kwargs):
        super(NovaPoshtaForm, self).__init__(*args, **kwargs)
        try:
            if 'city' in self.data:
                self.fields['city'].queryset = Settlement.objects.filter(
                    pk=self.data['city'])
            if 'street' in self.data:
                self.fields['street'].queryset = Warehouse.objects.filter(
                    pk=self.data['street'])
        except (ValueError, TypeError):
            pass

    def clean(self):
        data = super(NovaPoshtaForm, self).clean()
        data.pop('search_city')
        return data

