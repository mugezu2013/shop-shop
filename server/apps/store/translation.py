from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from .models import (
    Category, Brand, Product, Color, Size, ProductVariant, Image, Basket,
    MethodDelivery, MethodPayment, Order, Payment
)


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'alt', 'description')


@register(Brand)
class BrandTranslationOptions(TranslationOptions):
    fields = ('alt', 'description')


@register(Product)
class ProductTranslationOptions(TranslationOptions):
    fields = ('title', 'alt', 'description', 'content')


@register(Color)
class ColorTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


@register(Size)
class SizeTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


@register(ProductVariant)
class ProductVariantTranslationOptions(TranslationOptions):
    fields = ('vendor_code',)


@register(Image)
class ProductVariantTranslationOptions(TranslationOptions):
    fields = ('alt',)


#     ---------------------

@register(Basket)
class BasketTranslationOptions(TranslationOptions):
    fields = ('status',)


@register(MethodDelivery)
class MethodDeliveryTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'alt',)


@register(MethodPayment)
class MethodPaymentTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'alt', 'status',)


@register(Order)
class OrderTranslationOptions(TranslationOptions):
    fields = ('status', 'payment_status',)


@register(Payment)
class PaymentTranslationOptions(TranslationOptions):
    fields = ('payment_status',)
