from django import forms
from django.db.models import Min
from django.utils.translation import ugettext_lazy as _
from django_filters import (ModelMultipleChoiceFilter, RangeFilter,
                            OrderingFilter, ModelChoiceFilter)
from django_filters.rest_framework import FilterSet
from django_filters.widgets import RangeWidget

from .models import Color, ProductVariant, Size, Brand, Product
from ..core.core_utils import get_USD_money
from ..novaposhta.models import Warehouse


class ProductVariantFilter(FilterSet):
    color = ModelMultipleChoiceFilter(label=_('Цвет'),
                                      queryset=Color.objects.all(),
                                      widget=forms.CheckboxSelectMultiple)

    price = RangeFilter(label=_('Цена'), method='price_method',
                        widget=RangeWidget(attrs={'type': "number"}))
    size = ModelMultipleChoiceFilter(label=_('Размер'),
                                     queryset=Size.objects.all(),
                                     widget=forms.CheckboxSelectMultiple)
    product__brand = ModelMultipleChoiceFilter(label=_('Размер'),
                                               queryset=Brand.objects.all(),
                                               widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, request, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    def price_method(self, queryset, name, value):
        if value.start is not None and value.stop is not None:
            qs = queryset.filter(price__range=(
                get_USD_money(self.request, value.start),
                get_USD_money(self.request, value.stop)))
        elif value.start is not None:
            qs = queryset.filter(
                price__gte=get_USD_money(self.request, value.start))
        elif value.stop is not None:
            qs = queryset.filter(
                price__lte=get_USD_money(self.request, value.stop))
        else:
            qs = queryset
        return qs

    SORT_BY_CHOICES = (
        ('price', _(
            'От дешевых к дорогим:'
        )),
        ('-price', _(
            'От дорогих к дешевым '
        )),
        ('created_at', _(
            'Новинки'
        )),
        ('product__average_rating', _(
            'Популярные'
        ))
    )

    def ordering_method(self, queryset, name, value):
        return queryset

    sort = OrderingFilter(
        label=_(
            'Сортировка по:'
        ),
        choices=SORT_BY_CHOICES,
        method='ordering_method'
    )

    class Meta:
        model = ProductVariant
        fields = ('color', 'price', 'size', 'product__brand')


class ProductFilter(FilterSet):
    SORT_BY_CHOICES = (
        ('price', _(
            'От дешевых к дорогим:'
        )),
        ('-price', _(
            'От дорогих к дешевым '
        )),
        ('created_at', _(
            'Новинки'
        )),
        ('product__average_rating', _(
            'Популярные'
        ))
    )

    def ordering_method(self, queryset, name, value):
        key_order = value[0]
        qs = queryset
        print(key_order)
        if key_order is None:
            qs = qs.order_by(
                '-average_rating')
        elif key_order == 'price':
            qs = qs.annotate(
                Min('variants__price')).order_by(
                'variants__price__min')
        elif key_order == '-price':
            qs = qs.annotate(
                Min('variants__price')).order_by(
                '-variants__price__min')
        elif key_order == 'created_at':
            qs = qs.order_by(
                '-created_at')
        elif key_order == 'product__average_rating':
            qs = qs.order_by(
                '-average_rating')
        return qs

    sort = OrderingFilter(
        label=_(
            'Сортировка по:'
        ),
        choices=SORT_BY_CHOICES,
        method='ordering_method'
    )

    class Meta:
        model = Product
        fields = ('title',)


class Novaposhta(FilterSet):
    city_description_ru = ModelChoiceFilter(label=_('Отделение'),
                                            queryset=Warehouse.objects.filter(
                                                city_ref='db5c8892-391c-11dd-90d9-001a92567626'),
                                            widget=forms.Select)

    class Meta:
        model = Warehouse
        fields = ('city_description_ru',)

    def __init__(self, *args, request, ref, **kwargs):
        super(Novaposhta, self).__init__(*args, **kwargs)
        # self.city_description_ru.queryset = Warehouse.objects.filter(
        #     city_ref=ref)
        # print(self.)
