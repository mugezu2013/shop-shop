from constance import config
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from ok_images.fields import OptimizedImageField
from versatileimagefield.fields import PPOIField

from shared.models.active import ActiveMixin


class MethodDelivery(ActiveMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'),
                             blank=True)
    description = models.CharField(max_length=255, verbose_name=_('Описание'),
                                   blank=True)

    image = OptimizedImageField(
        _('Image'),
        ppoi_field='ppoi',
        upload_to='delivery/%d%m%y',
        image_sizes='product',
        blank=True,
    )
    ppoi = PPOIField(
        verbose_name=_('Картинка')
    )
    alt = models.CharField(blank=True, max_length=255)
    price_delivery = models.DecimalField(max_digits=10, decimal_places=2,
                                         verbose_name=_('Стоимость доставки'))

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Способ доставки')
        verbose_name_plural = _('Способы доставки')

    def __str__(self):
        return self.title


class MethodPayment(ActiveMixin, models.Model):
    STATUS = [
        (_('Оплата картой'), (
            ('LiqPAY', 'LiqPAY'),
        )),
        (_('Оффлайн'), (
            ('C.O.D.', _('Наложным платежом')),
            ('cash', _('Наличными')),
        )),
    ]
    title = models.CharField(max_length=255, verbose_name=_('Название'),
                             blank=True)
    description = models.CharField(max_length=255, verbose_name=_('Описание'),
                                   blank=True)
    image = OptimizedImageField(
        _('Image'),
        ppoi_field='ppoi',
        upload_to='products/%d%m%y',
        image_sizes='product',
        blank=True,
        default=config.DEFAULT_IMAGE_404
    )
    ppoi = PPOIField(
        verbose_name=_('Картинка')
    )
    alt = models.CharField(blank=True, max_length=255)

    status = models.CharField(choices=STATUS, max_length=20,
                              default='new',
                              verbose_name=_('Статус'))

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Способ оплаты')
        verbose_name_plural = _('Способы оплаты')

    def __str__(self):
        return self.title


class Order(models.Model):
    STATUS = (
        ('new', 'New'),
        ('confirmed', 'Confirmed'),
        ('sent', 'Sent'),
        ('done', 'Done'),
        ('canceled', 'Canceled'))

    STATUS_PAYMENT = (
        ('paid', 'Paid'),
        ('not paid', 'Not paid'))
    number_order = models.CharField(max_length=254, unique=True,
                                    verbose_name=_('Код заказа'))
    status = models.CharField(choices=STATUS, max_length=20,
                              default=STATUS[0][0], blank=True,
                              verbose_name=_('Статус'))
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             null=True, blank=True,
                             related_name='order',
                             related_query_name='order',
                             verbose_name=_('Пользователь'))

    basket = models.ForeignKey('Basket', on_delete=models.CASCADE,
                                  related_name='order', blank=True,
                                  related_query_name='order',
                                  verbose_name=_('Корзина'))

    delivery = models.ForeignKey(MethodDelivery, blank=True, null=True,
                                 related_name='orders',
                                 related_query_name='orders',
                                 on_delete=models.CASCADE,
                                 verbose_name=_('Способ доставки')
                                 )
    payment_method = models.ForeignKey(MethodPayment, blank=True, null=True,
                                       related_name='orders',
                                       related_query_name='orders',
                                       on_delete=models.CASCADE,
                                       verbose_name=_('Способ оплаты')
                                       )
    payment_status = models.CharField(choices=STATUS_PAYMENT, max_length=20,
                                      default='not paid',
                                      verbose_name=_('Статус оплаты'))

    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Создано'))
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=_('Обновлено'))

    class Meta(object):
        ordering = ['-updated_at', ]
        verbose_name = _('Заказ')
        verbose_name_plural = _('Заказы')


class OrderProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE,
                              related_name='variant_order',
                              related_query_name='variant_order',
                              verbose_name=_('Заказ'))
    product_variant = models.ForeignKey('ProductVariant',
                                        related_name='variant_order',
                                        related_query_name='variant_order',
                                        on_delete=models.CASCADE,
                                        verbose_name=_('Продукт'))
    price_product = models.DecimalField(max_digits=10, decimal_places=2,
                                        default=0,
                                        verbose_name=_('Стоимость продукта'))
    count = models.PositiveSmallIntegerField(default=1,
                                             verbose_name=_('Количество'))
    price_total = models.DecimalField(max_digits=10, decimal_places=2,
                                      default=0,
                                      verbose_name=_('Общая стоимость'))

    class Meta:
        ordering = ['-order', ]
        verbose_name = _('Продукт заказа')
        verbose_name_plural = _('Продукты заказа')
        unique_together = ['order', 'product_variant', ]

    def __str__(self):
        return '{}({})'.format(self.product_variant.vendor_code, self.count)


class Pay(models.Model):
    order = models.OneToOneField(Order, on_delete=models.CASCADE,
                                 related_name='pay',
                                 related_query_name='pay',
                                 verbose_name=_('Заказ'))
    price_products = models.DecimalField(max_digits=10, decimal_places=2,
                                         blank=True, default=0,
                                         verbose_name=_('Стоимость продуктов'))
    price_delivery = models.DecimalField(max_digits=10, decimal_places=2,
                                         blank=True, default=0,
                                         verbose_name=_('Стоимость доставки'))
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True,
                                default=0,
                                verbose_name=_('Общая стоимость'))

    class Meta:
        ordering = ['-price', ]
        verbose_name = _('Оплата')
        verbose_name_plural = _('Оплата')


class AddressDelivery(models.Model):
    order = models.OneToOneField(Order, on_delete=models.CASCADE,
                                 related_name='address',
                                 related_query_name='address',
                                 verbose_name=_('Заказ'))
    phone = models.CharField(max_length=15, blank=True,
                             verbose_name=_("Телефон"),
                             validators=
                             [RegexValidator(
                                 r"[+]?\d{9,15}",
                                 message=_(
                                     'Не соответствует формату телефона'))])
    first_name = models.CharField(max_length=254,
                                  verbose_name=_('Имя'))
    last_name = models.CharField(max_length=254,
                                 verbose_name=_('Фамилия'))
    email = models.EmailField(max_length=254,
                              verbose_name='Email')

    country = models.CharField(max_length=254, blank=True,
                               verbose_name=_('Страна'))
    index = models.CharField(max_length=10, blank=True,
                             validators=
                             [RegexValidator(
                                 r"^\d{5,10}$",
                                 message=_(
                                     'Не соответствует формату индекса'))],
                             verbose_name=_('Индекс'))
    city = models.CharField(max_length=254, blank=True,
                            verbose_name=_('Город'))
    street = models.CharField(max_length=254, blank=True,
                              verbose_name=_('Улица'))
    number_house = models.CharField(max_length=10, blank=True,
                                    verbose_name=_('Номер дома'))
    number_apartment = models.CharField(max_length=10, blank=True,
                                        verbose_name=_('Номер квартиры'))

    class Meta(object):
        verbose_name = _('Адрес доставки')
        verbose_name_plural = _('Адреса доставки')


class Payment(models.Model):
    STATUS_PAYMENT = (
        ('paid', 'Paid'),
        ('not paid', 'Not paid')
    )
    order = models.OneToOneField(Order, on_delete=models.CASCADE,
                                 related_name='payment',
                                 related_query_name='payment',
                                 verbose_name=_('Корзина'))
    method_payment = models.ForeignKey(MethodPayment, on_delete=models.CASCADE,
                                       related_name='payments',
                                       related_query_name='payments',
                                       verbose_name=_('Метод оплаты'))
    price_products = models.DecimalField(max_digits=10, decimal_places=2,
                                         default=0,
                                         verbose_name=_('Стоимость продуктов'))
    price_delivery = models.DecimalField(max_digits=10, decimal_places=2,
                                         default=0,
                                         verbose_name=_('Стоимость доставки'))
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True,
                                default=0,
                                verbose_name=_('Общая стоимость'))
    money_received = models.DecimalField(max_digits=10, decimal_places=2,
                                         verbose_name=_('Денег получено'))
    payment_status = models.CharField(choices=STATUS_PAYMENT, max_length=50,
                                      verbose_name=_('Статус платежа'))
    request_data = models.TextField(verbose_name='Request Data', blank=True)
    response_data = models.TextField(verbose_name='Response Data')
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Создано'))
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=_('Обновлено'))

    class Meta:
        ordering = ['-updated_at', ]
        verbose_name = _('Платеж')
        verbose_name_plural = _('Платежи')
