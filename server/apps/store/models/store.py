from ckeditor.fields import RichTextField
from colorfield.fields import ColorField
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from ok_images.fields import OptimizedImageField
from versatileimagefield.fields import PPOIField

from shared.models import SlugifyMixin
from shared.models.active import ActiveMixin
from shared.models.timestamps import TimestampsMixin


class Category(SlugifyMixin, ActiveMixin, MPTTModel):
    name = models.CharField(max_length=100, verbose_name=_('Имя'))
    description = models.CharField(max_length=255, verbose_name=_('Описание'))
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True,
                            limit_choices_to={'parent__isnull': True},
                            blank=True, related_name='children')
    show = models.BooleanField(max_length=254, default=True, blank=True,
                               verbose_name=_('Показать'))
    preview = OptimizedImageField(
        _('Превью'),
        ppoi_field='ppoi',
        upload_to='categories/%d%m%y',
        image_sizes='product',
        blank=True,
    )
    ppoi = PPOIField(
        verbose_name=_('Превью')
    )
    alt = models.CharField(blank=True, max_length=255)

    class Meta:
        verbose_name = _('Категория')
        verbose_name_plural = _('Категории')

    class MPTTMeta:
        order_insertion_by = ['name']

    def save(self, *args, **kwargs):
        if self.parent is not None:
            if self.parent.level == 1:
                raise ValueError(_('Достигнута максимальная вложенность!'))
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        if self.parent is not None:
            return '{}({})'.format(self.name, self.parent)
        else:
            return self.name


class Brand(SlugifyMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'))
    description = models.CharField(max_length=255, verbose_name=_('Описание'))
    preview = OptimizedImageField(
        _('Превью'),
        ppoi_field='ppoi',
        upload_to='brands/%d%m%y',
        image_sizes='product',
        blank=True,
    )
    ppoi = PPOIField(
        verbose_name=_('Превью')
    )
    alt = models.CharField(blank=True, max_length=255)

    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    url = models.URLField(blank=True)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Бренд')
        verbose_name_plural = _('Бренды')

    def __str__(self):
        return self.title


class ProductQuerySet(models.QuerySet):
    def top_product(self, count=None):
        return self.filter(top_product=True)[:count]

    def top_buy(self, count=None):
        return self.filter(top_buy=True)[:count]


class Product(SlugifyMixin, TimestampsMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'))
    description = models.CharField(max_length=255, verbose_name=_('Описание'))
    content = RichTextField(null=True, verbose_name=_('Контент'))
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE,
                              verbose_name=_('Бренд'))
    recommended_products = models.ManyToManyField('self', symmetrical=False,
                                                  blank=True,
                                                  verbose_name=_(
                                                      'Рекомендуемые товары'))
    category = models.ForeignKey(Category, on_delete=models.CASCADE,
                                 default=None, blank=True,
                                 limit_choices_to={
                                     'parent__isnull': False},
                                 verbose_name=_('Категории'))
    average_rating = models.FloatField(blank=True, default=0, max_length=2,
                                       verbose_name=_('Средний рейтинг'))
    count_reviews = models.PositiveIntegerField(blank=True, default=0,
                                                verbose_name=_(
                                                    'Количество отзывов'))
    news = models.BooleanField(max_length=254, default=True, blank=True,
                               verbose_name=_('Новинка'))
    top_buy = models.BooleanField(max_length=254, default=False, blank=True,
                                  verbose_name=_('Топ продаж'))
    top_product = models.BooleanField(max_length=254, default=False, blank=True,
                                      verbose_name=_('Топ товар'))

    objects = ProductQuerySet.as_manager()
    preview = OptimizedImageField(
        _('Image'),
        ppoi_field='ppoi',
        upload_to='products/%d%m%y',
        image_sizes='product',
        blank=True,
    )
    ppoi = PPOIField(
        verbose_name=_('Превью')
    )
    alt = models.CharField(blank=True, max_length=255)

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Товар')
        verbose_name_plural = _('Товары')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('store:product',
                       kwargs={'category': self.category.parent.slug,
                               'sub_category': self.category.slug,
                               'slug': self.slug})


class Color(SlugifyMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'))
    description = models.CharField(max_length=255, verbose_name=_('Описание'),
                                   blank=True)
    color = ColorField(default='#FF0000')

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Цвет')
        verbose_name_plural = _('Цвета')

    def __str__(self):
        return self.title


class Size(SlugifyMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'))
    description = models.CharField(max_length=255, verbose_name=_('Описание'))

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Размер')
        verbose_name_plural = _('Размеры')

    def __str__(self):
        return self.title


class ProductVariant(ActiveMixin, models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name='variants',
                                related_query_name='variants',
                                verbose_name=_('Товар'))
    vendor_code = models.CharField(max_length=254, verbose_name=_('Артикул'))
    color = models.ForeignKey(Color, verbose_name=_('Цвет'),
                              related_name='variants',
                              related_query_name='variants',
                              on_delete=models.CASCADE)
    size = models.ForeignKey(Size, verbose_name=_('Размер'),
                             related_name='variants',
                             related_query_name='variants',
                             on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2,
                                verbose_name=_('Цена'))

    price_sale = models.DecimalField(max_digits=10, decimal_places=2,
                                     blank=True, default=0,
                                     verbose_name=_('Цена по акции'))

    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Создано'))
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=_('Обновлено'))
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        unique_together = ['color', 'size', 'product']
        ordering = ['order', 'product', ]
        verbose_name = _('Вариант товара')
        verbose_name_plural = _('Варианты товара')

    def __str__(self):
        return str(self.vendor_code)


class Image(models.Model):
    image = OptimizedImageField(
        _('Image'),
        ppoi_field='ppoi',
        upload_to='products/%d%m%y',
        image_sizes='product',
    )
    ppoi = PPOIField(
        verbose_name=_('Картинка')
    )
    alt = models.CharField(blank=True, max_length=254)
    product_variant = models.ForeignKey(
        ProductVariant, null=True,
        on_delete=models.CASCADE,
        related_name='images',
        related_query_name='images',
    )
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Изображение')
        verbose_name_plural = _('Изображения')


class ReviewQuerySet(models.QuerySet):

    def published(self, count=None):
        return self.filter(status='published')[:count]


class Review(models.Model):
    RATING_VALUE = ((1, 1), (2, 2), (3, 3), (4, 4), (5, 5))
    STATUS_REVIEW = (
        ('published', 'Published'), ('new', 'New'), ('canceled', 'Canceled'))
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                related_name='reviews',
                                related_query_name='reviews', default=None)

    rating = models.IntegerField(choices=RATING_VALUE,
                                 verbose_name=_('Рейтинг'))
    comment = models.TextField(unique=False,
                               verbose_name=_('Коментарий'))
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             null=True, blank=True,
                             related_name='reviews',
                             related_query_name='reviews',
                             verbose_name=_('Пользователь'))
    email = models.EmailField(verbose_name=_('Електронная почта'))
    user_name = models.CharField(max_length=254,
                                 verbose_name=_('Имя пользователя'))
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Создано'))
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=_('Обновлено'))
    status = models.CharField(choices=STATUS_REVIEW, max_length=20,
                              default='new',
                              verbose_name=_('Статус'))
    reason_for_rejection = models.CharField(max_length=254, blank=True,
                                            verbose_name=_(
                                                'Причина отклонения'))

    objects = ReviewQuerySet.as_manager()

    class Meta:
        ordering = ['-created_at', ]
        verbose_name = _('Отзыв')
        verbose_name_plural = _('Отзывы')

    def __str__(self):
        return 'От {} про {}'.format(self.user_name, self.product)
