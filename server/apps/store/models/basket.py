from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class BasketQuerySet(models.QuerySet):
    def open_last(self):
        return self.filter(status='open').order_by('-updated_at').first()

    def open(self):
        return self.filter(status='open').order_by('-updated_at')


class Basket(models.Model):
    STATUS = (('open', 'Open'), ('close', 'Close'))
    status = models.CharField(choices=STATUS, max_length=20,
                              default='open',
                              verbose_name=_('Статус'))
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             null=True, default=None, blank=True,
                             related_name='basket',
                             related_query_name='basket',
                             verbose_name=_('Пользователь'))
    session_key = models.CharField(max_length=254)
    count = models.PositiveSmallIntegerField(default=0,
                                             verbose_name=_(
                                                 'Количество товаров'))
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0,
                                verbose_name=_('Стоимость'))
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Создано'))
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=_('Обновлено'))

    objects = BasketQuerySet.as_manager()

    class Meta:
        ordering = ['-created_at', ]
        verbose_name = _('Корзина')
        verbose_name_plural = _('Корзины')

    def __str__(self):
        return self.session_key


class BasketProduct(models.Model):
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE,
                               related_name='products',
                               related_query_name='products',
                               verbose_name=_('Корзина'))

    product_variant = models.ForeignKey('ProductVariant',
                                        related_name='baskets',
                                        related_query_name='baskets',
                                        on_delete=models.CASCADE,
                                        verbose_name=_('Продукт'))
    count = models.PositiveSmallIntegerField(default=0,
                                             verbose_name=_('Количество'))

    class Meta:
        unique_together = ('basket', 'product_variant',)
        ordering = ['-basket', ]
        verbose_name = _('Продукт корзины')
        verbose_name_plural = _('Продукты корзины')

    def __str__(self):
        return '{}({})'.format(self.product_variant.vendor_code, self.count)
