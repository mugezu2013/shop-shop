from django.contrib.auth.models import User

from app.celery import app
from app.postie_custom import send_mail_custom
from apps.core.selectors import get_recipients_email


@app.task
def new_review(url_review):
    qs = get_recipients_email('new_review')

    send_mail_custom(
        event=qs.sample_email,
        recipients=qs.get_users(),
        context={
            'link_review': url_review,
        },
    )


@app.task
def new_order(number_order, user_id):
    user = User.objects.get(id=user_id)

    send_mail_custom(
        event='new_order',
        recipients=[user.email, ],
        context={
            'first_name': user.first_name,
            'number_order': number_order,
        },
    )


@app.task
def new_order_for_recipients(url_order):
    qs = get_recipients_email('new_order_for_recipients')

    send_mail_custom(
        event=qs.sample_email,
        recipients=qs.get_users(),
        context={
            'link': url_order,
        },
    )
