from django.db import IntegrityError
from django.db.models import (
    Sum, F, DecimalField, ExpressionWrapper, PositiveIntegerField
)

from apps.store.models import Basket, BasketProduct


def connect_basket(basket_session, request):
    if request.session.session_key is None:
        request.session.cycle_key()
    basket_user = get_basket_user(request)
    if basket_user is None:
        basket_session.user = request.user
        basket_session.session_key = request.session.session_key
        basket_session.save()
        return basket_session
    if basket_user and basket_session:
        try:
            basket_user.products.add(*(basket_session.products.all()))
        except IntegrityError:
            pass

        math_basket(basket_user)
        close_basket(basket_session)

    return basket_user


def get_basket_session(request):
    if request.session.session_key is None:
        request.session.cycle_key()
    basket = Basket.objects.open().filter(
        session_key=request.session.session_key).first()
    if basket is None:
        basket = Basket(session_key=request.session.session_key)
        basket.save()

    return basket


def get_basket_user(request):
    if request.user.is_authenticated:
        user = request.user

        basket_user = user.basket.open_last()
        if basket_user is None:
            basket_user = Basket(user=user,
                                 session_key=request.session.session_key).save()
    else:
        basket_user = None
    return basket_user


def get_or_create_basket(request):
    basket_user = get_basket_user(request)
    if basket_user:
        basket = basket_user
    else:
        basket = get_basket_session(request)
    return basket


def get_basket_product(basket: Basket):
    qs = BasketProduct.objects.filter(
        basket=basket).prefetch_related(
        "product_variant__product", 'product_variant__color',
        'product_variant__size')
    return qs


def get_product_in_basket(basket: Basket, product):
    if basket.products.filter(product_variant=product).exists():
        return basket.products.get(product_variant=product)
    else:
        return None


def update_product_in_basket(basket: Basket, product, count):
    product_in_basket = get_product_in_basket(basket, product)

    if count != "0":
        if product_in_basket is not None:
            product_in_basket.count = count
            product_in_basket.save()
        else:
            BasketProduct(product_variant=product,
                          basket=basket, count=count).save()

    if count == "0" and product_in_basket is not None:
        product_in_basket.delete()


def math_basket(basket: Basket):
    basket.count = basket.products.aggregate(
        total_count=ExpressionWrapper(
            Sum('count'),
            output_field=PositiveIntegerField()
        ))['total_count'] or 0
    basket.price = basket.products.aggregate(
        total_price=ExpressionWrapper(
            Sum(F('product_variant__price') * F('count')),
            output_field=DecimalField(max_digits=10, decimal_places=2)
        ))['total_price'] or 0
    basket.save()
    return basket


def close_basket(basket: Basket):
    basket.status = Basket().STATUS[1][0]
    basket.save()
