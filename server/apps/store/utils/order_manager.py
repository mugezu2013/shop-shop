from django.db.models import Sum, ExpressionWrapper, F, DecimalField
from django.db.transaction import atomic
from django.forms import BaseForm

from apps.accounts.forms import UserFastRegistrationForm
from apps.store.models import (
    Order, Basket, OrderProduct, AddressDelivery, Pay, Payment

)
from apps.store.utils import close_basket


@atomic
def add_products_order_with_basket(order: Order, basket: Basket):
    for products_basket in basket.products.all():
        price_total = (
                products_basket.product_variant.price * products_basket.count
        )

        order_product, create = OrderProduct.objects.get_or_create(
            order=order,
            product_variant=products_basket.product_variant,
        )
        order_product.price_product = products_basket.product_variant.price
        order_product.count = products_basket.count
        order_product.price_total = price_total
        order_product.save()


@atomic
def create_address_delivery(order, user_form: UserFastRegistrationForm,
                            address_form: BaseForm):
    address = AddressDelivery.objects.filter(order=order).first()
    if address is not None:
        address.delete()

    return AddressDelivery(
        order=order,
        **user_form.clean_data_for_address,
        **address_form.cleaned_data,
    ).save()


def create_pay(order: Order):
    pay = Pay.objects.filter(order=order).first()
    if pay is not None:
        pay.delete()

    price_products = order.variant_order.aggregate(
        total_price=ExpressionWrapper(
            Sum(F('product_variant__price') * F('count')),
            output_field=DecimalField(max_digits=10, decimal_places=2)
        ))['total_price']
    data = {
        'price_products': price_products,
        'price_delivery': order.delivery.price_delivery,
        'price': price_products + order.delivery.price_delivery
    }
    return Pay(order=order, **data).save()


@atomic()
def formation_order(order, basket, user_form: UserFastRegistrationForm,
                    address_form: BaseForm, request):
    if user_form.cleaned_data['create']:
        user = user_form.save_and_send_mail(request=request)
        basket.user = user
        basket.save()

    order.user = basket.user
    order.basket = basket
    order.save()
    add_products_order_with_basket(order, basket)

    create_address_delivery(order, user_form, address_form)
    create_pay(order)
    return order


def create_payment(order: Order, data):
    commission = 0
    try:
        commission = data['receiver_commission']
    except KeyError:
        pass

    payment = Payment(
        order=order,
        method_payment=order.payment_method,
        price_products=order.pay.price_products,
        price_delivery=order.pay.price_delivery,
        price=order.pay.price,
        money_received=data['amount'] - commission,
        payment_status=data['status'].upper(),
        response_data=data
    )
    payment.save()
    return payment


@atomic()
def change_order_payment_status(order):
    order.status = Order.STATUS[1][0]
    order.payment_status = Order.STATUS_PAYMENT[0][0]
    close_basket(order.basket)
    order.save()
