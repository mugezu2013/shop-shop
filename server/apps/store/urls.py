from django.urls import path, include

from .views import (
    ProductListView, ProductDetailView, AddProductInBasketView, BasketView,
    OrderingView, load_warehouse, load_cities, OrderResult, OrderSuccess,
    PayView, PayCallbackView, OrderPaid, MainStorePageView
)

urlpatterns = [
    path('store/', include(([
        path('', MainStorePageView.as_view(), name='main'),
        path('basket/add/<int:pk>/', AddProductInBasketView.as_view(),
             name='add_basket'),
        path('basket/', BasketView.as_view(),
             name='basket'),
        path('ordering/', OrderingView.as_view(),
             name='ordering'),
        path('ordering/result/<str:number_order>/', OrderResult.as_view(),
             name='ordering-result'),
        path('ordering/success', OrderSuccess.as_view(),
             name='ordering-success'),
        path('ordering/paid', OrderPaid.as_view(),
             name='ordering-paid'),
        path('pay/<order>/', PayView.as_view(), name='pay-view'),
        path('pay-callback/', PayCallbackView.as_view(), name='pay-callback'),

        path('ajax/load-warehouses/', load_warehouse,
             name='ajax_load_warehouses'),
        path('ajax/load-cities/', load_cities, name='ajax_load_cities'),
        path('<slug:category>/', ProductListView.as_view(),
             name='main_category'),
        path('<slug:category>/<slug:sub_category>/',
             ProductListView.as_view(),
             name='sub_category'),
        path('<slug:category>/<slug:sub_category>/<slug:slug>/',
             ProductDetailView.as_view(), name='product'),
    ])))
]
