from django.db.models import Avg
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.store.models import Order, Review


@receiver(post_save, sender=Order)
def init_number_order(sender, instance, **kwargs):
    print('instance' * 10, instance.number_order)
    if instance.number_order:
        print('instance' * 10, instance.number_order)
    else:
        instance.number_order = 'OR-{}-{}'.format(
            instance.created_at.strftime('%d%m%y'), instance.pk)
        instance.save()


@receiver(post_save, sender=Review)
def update_rating(sender, instance, **kwargs):
    if instance.status == 'published':
        product = instance.product
        product.average_rating = (
            Review.published.filter(product=product).aggregate(
                Avg('rating'))['rating__avg']
        )
        product.save(update_fields=['average_rating'])
