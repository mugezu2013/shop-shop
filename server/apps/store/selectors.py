from .models import ProductVariant

ACTIVE_ORDERED_VARIANT_PRODUCTS = ProductVariant.objects.select_related(
            'color', 'size').filter(is_active=True).order_by('price')

SALE_PRODUCT_VARIANT = ACTIVE_ORDERED_VARIANT_PRODUCTS.exclude(
    price_sale=0)