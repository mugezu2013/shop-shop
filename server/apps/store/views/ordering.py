from constance import config
from django.db.models import Q
from django.db.transaction import atomic
from django.forms import modelformset_factory
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.utils.translation import pgettext
from django.utils.translation import ugettext_lazy as _
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django_jinja.views.generic import DetailView
from liqpay import LiqPay

from apps.accounts.forms import UserFastRegistrationForm
from apps.novaposhta.models import Warehouse, Settlement
from apps.store.utils.basket_manager import (
    get_or_create_basket, get_basket_product, close_basket
)
from shared.mixins import BreadcrumbMixin
from ..forms import BasketVariantProductForm, OrderingForm, NovaPoshtaForm
from ..models import BasketProduct, Order
from ..tasks import new_order, new_order_for_recipients
from ..utils import (
    formation_order, create_payment, change_order_payment_status,
    CONFIRMED_STATUS_LIQPAY
)


class OrderingView(BreadcrumbMixin, DetailView):
    object = None
    template_name = 'store/ordering.jinja'
    basket_variant_product_form_set = (
        modelformset_factory(BasketProduct,
                             form=BasketVariantProductForm,
                             can_delete=True, max_num=0, ))
    basket_form_set = None
    ordering_form = None
    user_form = None
    warehouse_form = None
    redirect_page = reverse_lazy('store:main')

    def setup(self, request, *args, **kwargs):
        super(OrderingView, self).setup(request=request, args=args,
                                        kwargs=kwargs)
        self.basket = get_or_create_basket(self.request)
        if self.basket.products.count() > 0:
            qs = get_basket_product(self.basket)
            self.basket_form_set = self.basket_variant_product_form_set(
                queryset=qs)

    def get_breadcrumbs(self):
        breadcrumbs = super(OrderingView, self).get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=_('Оформление заказа')
        ))
        return breadcrumbs

    def get_object(self, queryset=None):
        return None

    def get_context_data(self, **kwargs):
        context = super(OrderingView, self).get_context_data(**kwargs)
        context['form_set'] = self.basket_form_set
        context['ordering_form'] = self.ordering_form
        context['warehouse_form'] = self.warehouse_form
        context['user_form'] = self.user_form

        return context

    def get(self, request, *args, **kwargs):
        if self.basket.products.count() > 0:
            self.ordering_form = OrderingForm()
            if request.user.is_authenticated:
                self.user_form = UserFastRegistrationForm(instance=request.user)
            else:
                self.user_form = UserFastRegistrationForm()
            self.warehouse_form = NovaPoshtaForm()
            return super(OrderingView, self).get(request=request, args=args,
                                                 kwargs=kwargs)
        else:
            return redirect('store:main')

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            self.user_form = UserFastRegistrationForm(request.POST,
                                                      instance=request.user)
        else:
            self.user_form = UserFastRegistrationForm(request.POST)

        self.ordering_form = OrderingForm(request.POST)
        self.warehouse_form = NovaPoshtaForm(request.POST)

        if (
                self.ordering_form.is_valid() and
                self.warehouse_form.is_valid() and
                self.user_form.is_valid()
        ):
            order = self.ordering_form.save(False)
            order = formation_order(
                order=order,
                basket=self.basket,
                user_form=self.user_form,
                address_form=self.warehouse_form,
                request=request,
            )

            return redirect('store:ordering-result',
                            number_order=order.number_order)
        else:
            return self.render_to_response(self.get_context_data())


class OrderResult(BreadcrumbMixin, DetailView):
    model = Order
    template_name = 'store/order_result.jinja'
    slug_url_kwarg = 'number_order'
    slug_field = 'number_order'

    def get_queryset(self):
        return super(OrderResult, self).get_queryset().select_related(
            'pay',
            'delivery',
            'payment_method',
            'user'
            # 'address'
        )

    def get_breadcrumbs(self):
        breadcrumbs = super(OrderResult, self).get_breadcrumbs()

        breadcrumbs.append(self.build_breadcrumb(
            title=_('Оформление заказа'),
            path=reverse_lazy('store:ordering')
        ))
        breadcrumbs.append(self.build_breadcrumb(
            title=_('Order')
        ))
        return breadcrumbs

    def post(self, request, *args, **kwargs):
        order = self.get_object()

        if order.payment_method.status == 'LiqPAY':
            print('liq')
            return redirect('store:pay-view', order=order.number_order)
        else:
            close_basket(order.basket)
            new_order.delay(order.number_order, order.user.id)
            url_order = request.build_absolute_uri(
                reverse('admin:store_order_change', args=(order.id,)))
            new_order_for_recipients.delay(url_order)

            return redirect('store:ordering-success')


class PayView(TemplateView):
    template_name = 'store/liqpay.jinja'

    def get(self, request, *args, **kwargs):
        liqpay = LiqPay(config.LIQPAY_PUBLIC_KEY, config.LIQPAY_PRIVATE_KEY)
        order = get_object_or_404(Order, number_order=kwargs.get('order'))
        # todo change if restart machine or deploy
        path = 'https://66fa-185-248-128-230.ngrok.io'
        path = path + reverse("store:pay-callback")
        print(path)
        params = {
            'action': 'pay',
            'amount': str(order.pay.price),
            'currency': 'UAH',
            'description': pgettext(
                'store.views.PayView:params(description)',
                'Payment description'
            ),
            'order_id': order.number_order,
            'version': '3',
            'sandbox': 1,  # sandbox mode, set to 1 to enable it
            'result_url': request.build_absolute_uri(
                reverse("store:ordering-paid")),
            # url to callback view
            'server_url': path,
        }
        signature = liqpay.cnb_signature(params)
        data = liqpay.cnb_data(params)
        return render(request, self.template_name,
                      {'signature': signature, 'data': data})


@method_decorator(atomic, name='dispatch')
@method_decorator(csrf_exempt, name='dispatch')
class PayCallbackView(View):

    def post(self, request, *args, **kwargs):
        print('post')
        liqpay = LiqPay(config.LIQPAY_PUBLIC_KEY, config.LIQPAY_PRIVATE_KEY)
        response_status = 200
        # Получаем данные
        data = request.POST.get('data')
        signature = request.POST.get('signature')
        data_decode = liqpay.decode_data_from_str(data)

        print('data_decode', data_decode, '\n', 'Signature:', signature)

        # Проверяем подлинность
        sign = liqpay.str_to_sign(
            config.LIQPAY_PRIVATE_KEY + data + config.LIQPAY_PRIVATE_KEY)
        if sign == signature:
            print('callback is valid')
            order = get_object_or_404(Order,
                                      number_order=data_decode['order_id'])

            create_payment(order, data_decode)
            print(data_decode['status'])
            if data_decode['status'] in CONFIRMED_STATUS_LIQPAY:
                change_order_payment_status(order)
                new_order.delay(order.number_order, order.user.id)
                url_order = request.build_absolute_uri(
                    reverse('admin:store_order_change', args=(order.id,)))
                new_order_for_recipients.delay(url_order)

        else:
            response_status = 404
        return HttpResponse(status=response_status)


class OrderSuccess(BreadcrumbMixin, TemplateView):
    template_name = 'store/info_page.jinja'
    extra_context = {
        'title': _('Order is processed'),
        'massage': _(
            'Наш менеджер свяжется с Вами. '
            'Вы можете отслеживать статус заказа в Вашем личном кабинете'
        )
    }

    def get_breadcrumbs(self):
        breadcrumbs = super().get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=self.extra_context['title']
        ))
        return breadcrumbs


class OrderPaid(BreadcrumbMixin, TemplateView):
    template_name = 'store/info_page.jinja'
    extra_context = {
        'title': _('Order has been paid'),
        'massage': _(
            'Вы можете отслеживать статус заказа в Вашем личном кабинете'
        )
    }

    def get_breadcrumbs(self):
        breadcrumbs = super().get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=self.extra_context['title']
        ))
        return breadcrumbs


def load_cities(request):
    search = request.GET.get('search')
    qs = Settlement.objects.filter(
        Q(description__icontains=search) | Q(
            description_ru__icontains=search))

    return render(request, 'store/hr/cities_dropdown_list_options.html',
                  {'cities': qs})


def load_warehouse(request):
    city_id = request.GET.get('city')
    city_ref = Settlement.objects.get(pk=city_id).ref
    warehouses = Warehouse.objects.filter(city_ref=city_ref)
    return render(request, 'store/hr/warehouse_dropdown_list_options.html',
                  {'warehouses': warehouses})
