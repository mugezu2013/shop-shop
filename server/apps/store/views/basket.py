from django.forms import modelformset_factory
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import RedirectView
from django_jinja.views.generic import DetailView

from apps.store.utils.basket_manager import (
    get_or_create_basket, update_product_in_basket, math_basket,
    get_basket_product)
from shared.mixins import BreadcrumbMixin
from ..forms import BasketVariantProductForm
from ..models import ProductVariant, BasketProduct


class AddProductInBasketView(RedirectView):

    def setup(self, request, *args, **kwargs):
        super(AddProductInBasketView, self).setup(request=request, args=args,
                                                  kwargs=kwargs)
        self.basket = get_or_create_basket(request)
        self.product_variant = get_object_or_404(ProductVariant,
                                                 pk=kwargs['pk'])

    def get(self, request, *args, **kwargs):
        update_product_in_basket(self.basket, self.product_variant,
                                 request.GET['count'])
        math_basket(self.basket)
        return redirect(request.META.get('HTTP_REFERER'))


class BasketView(BreadcrumbMixin, DetailView):
    template_name = 'store/basket.jinja'
    basket_variant_product_form_set = (
        modelformset_factory(BasketProduct,
                             form=BasketVariantProductForm,
                             can_delete=True, max_num=0, )
    )
    form_set = None
    redirect_page = reverse_lazy('store:main')

    def get_object(self, queryset=None):
        return get_or_create_basket(self.request)

    def get_breadcrumbs(self):
        breadcrumbs = super(BasketView, self).get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=_('Basket')
        ))
        return breadcrumbs

    def setup(self, request, *args, **kwargs):
        super(BasketView, self).setup(request=request, args=args,
                                      kwargs=kwargs)

        basket = self.get_object()
        print(basket)
        if basket.products.count() > 0:
            qs = get_basket_product(basket)
            self.form_set = self.basket_variant_product_form_set(queryset=qs)

    def get_context_data(self, **kwargs):
        context = super(BasketView, self).get_context_data(**kwargs)
        context['form_set'] = self.form_set

        return context

    def post(self, request, *args, **kwargs):
        form_set = self.basket_variant_product_form_set(request.POST)

        if form_set.is_valid():
            for form in form_set:
                basket_product = form.save(commit=False)
                if 'DELETE' in form.changed_data:
                    basket_product.delete()
                else:
                    if form.cleaned_data:
                        if form.cleaned_data['count'] == 0:
                            basket_product.delete()
                        else:
                            basket_product.save()
            update_basket = math_basket(self.get_object())
            if update_basket.count == 0:
                return redirect(self.redirect_page)
        return self.get(request, args, kwargs)
