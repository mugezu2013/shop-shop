from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Prefetch, Max, Min
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.translation import ugettext_lazy as _
from django_jinja.views.generic import ListView, DetailView
from seo.mixins.views import ModelInstanceViewSeoMixin, ViewSeoMixin

from shared.mixins import BreadcrumbMixin
from apps.store.utils.basket_manager import (get_or_create_basket,
                                             get_product_in_basket)
from ..filters import ProductVariantFilter, ProductFilter
from ..forms import ReviewForm, BasketProductForm
from ..models import Category, Product, Review, Color, Size
from ..selectors import ACTIVE_ORDERED_VARIANT_PRODUCTS
from ..tasks import new_review


class MainStorePageView(ListView):
    template_name = 'store/shop.jinja'
    queryset = Category.objects.active().filter(parent=None)

    def get(self, request, *args, **kwargs):
        return redirect('store:main_category',
                        category=Category.objects.active().first().slug)


class ProductListView(ViewSeoMixin, BreadcrumbMixin, ListView):
    paginate_by = 2
    model = Product
    template_name = 'store/shop.jinja'
    extra_context = {
        'title': _('Все товары')
    }

    def setup(self, request, *args, **kwargs):
        self.main_category_list = Category.objects.active().filter(parent=None)
        try:
            self.main_category = self.main_category_list.get(
                slug=kwargs['category'])
        except Category.DoesNotExist:
            raise Http404()
        except KeyError:
            self.main_category = self.main_category_list.first()
        try:
            if kwargs['sub_category']:
                self.sub_category = Category.objects.active().get(
                    slug=kwargs['sub_category'])
                self.sub_list_category = None
        except Category.DoesNotExist:
            raise Http404()
        except KeyError:
            self.sub_category = None
            self.sub_list_category = Category.objects.active().filter(
                parent=self.main_category)

        self.filter = ProductVariantFilter(request.GET,
                                           request=request,
                                           queryset=ACTIVE_ORDERED_VARIANT_PRODUCTS)
        super(ProductListView, self).setup(request, args, kwargs)

    def get_queryset(self):
        prefetch = Prefetch('variants', self.filter.qs)
        ids = list([obj.id for obj in self.filter.qs])

        if self.sub_category is not None:
            qs = Product.objects.filter(
                category=self.sub_category,
                variants__id__in=ids
            ).prefetch_related(prefetch).distinct()
        else:
            qs = Product.objects.filter(
                category__in=(self.main_category, *self.sub_list_category),
                variants__id__in=ids
            ).prefetch_related(prefetch).distinct()
        try:
            qs = ProductFilter(self.request.GET, queryset=qs).qs
        except MultiValueDictKeyError:
            pass
        return qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ProductListView, self).get_context_data(
            object_list=object_list, **kwargs)
        context['main_category_list'] = self.main_category_list
        context['main_category'] = self.main_category
        if self.sub_category is not None:
            context['breadcrumbs'].append(self.build_breadcrumb(
                title=self.main_category.name,
                path='/'.join(self.request.path.split('/')[:-2])))
            context['breadcrumbs'].append(self.build_breadcrumb(
                title=self.sub_category.name,
                path=self.request.path))
        else:
            context['breadcrumbs'].append(self.build_breadcrumb(
                title=self.main_category.name,
                path=self.request.path))
        context['sub_category_list'] = self.sub_list_category
        context['filter'] = self.filter
        qs = self.get_queryset().aggregate(Max('variants__price'),
                                           Min('variants__price'))
        context['max'] = qs['variants__price__max']
        context['min'] = qs['variants__price__min']
        context['count'] = self.get_queryset().count()
        return context


STATUS_FIELDS = ('active', 'open', 'close')


class ProductDetailView(ModelInstanceViewSeoMixin, BreadcrumbMixin, DetailView):
    template_name = 'store/product.jinja'
    model = Product
    review_form = None
    basket_product_form = None
    paginate_review = 3
    extra_context = {
        'title': _('Товар')
    }

    def get_colors_with_href(self, qs, get):
        variant_in_color = self.variants.filter(size=self.current_variant.size)

        param = ''
        for k, v in get.items():
            if k != 'variant':
                param = param + '&' + k + '=' + v

        result = dict.fromkeys(qs)
        for v in variant_in_color:
            if v == self.current_variant:
                result[v.color] = {'href': ('?variant=' + str(v.id) + param),
                                   'status': STATUS_FIELDS[0]}
                continue
            result[v.color] = {'href': ('?variant=' + str(v.id) + param),
                               'status': STATUS_FIELDS[1]}
        for c in qs:
            if result[c] is None:
                result[c] = {'href': (
                        '?variant=' + str(
                    self.variants.filter(color=c).first().id) + param),
                    'status': STATUS_FIELDS[2],
                }
        return result

    def get_sizes_with_href(self, qs, get):
        variant_in_size = self.variants.filter(color=self.current_variant.color)

        param = ''
        for k, v in get.items():
            if k != 'variant':
                param = param + '&' + k + '=' + v

        result = dict.fromkeys(qs)
        for v in variant_in_size:
            if v == self.current_variant:
                result[v.size] = {'href': ('?variant=' + str(v.id) + param),
                                  'status': STATUS_FIELDS[0],
                                  }
                continue
            result[v.size] = {'href': ('?variant=' + str(v.id) + param),
                              'status': STATUS_FIELDS[1],
                              }
        for s in qs:
            if result[s] is None:
                result[s] = {'href': (
                        '?variant=' + str(
                    self.variants.filter(size=s).first().id) + param),
                    'status': STATUS_FIELDS[2],
                }
        return result

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context['current_variant'] = self.current_variant
        context['color_list'] = self.color_list
        context['size_list'] = self.size_list
        user = self.request.user
        if user.is_authenticated:
            self.review_form = ReviewForm(initial={'email': user.email,
                                                   'user_name': user.first_name})
        else:
            if self.request.POST:
                self.review_form = ReviewForm(self.request.POST)
            else:
                self.review_form = ReviewForm()
        context['review_form'] = self.review_form
        self.basket = get_or_create_basket(self.request)
        product = get_product_in_basket(self.basket, self.current_variant)
        if product is not None:
            context['basket_product_form'] = BasketProductForm(
                initial={'count': product.count, 'in_basket': True})
        else:
            context['basket_product_form'] = BasketProductForm(
                initial={'count': 1, 'in_basket': False})

        context['breadcrumbs'].append(self.build_breadcrumb(
            title=self.object.category.parent.name,
            path='/'.join(self.request.path.split('/')[:-3])))
        context['breadcrumbs'].append(self.build_breadcrumb(
            title=self.object.category.name,
            path='/'.join(self.request.path.split('/')[:-2])))
        context['breadcrumbs'].append(self.build_breadcrumb(
            title=self.object,
            path=self.request.path))

        review_list = Review.objects.published().filter(
            product=self.current_variant.product)
        self.paginator = Paginator(review_list, self.paginate_review)
        self.page_number = self.request.GET.get(
            'page') if self.request.GET.get(
            'page') is not None else 1
        page_obj = self.paginator.get_page(self.page_number)
        context['page_obj'] = page_obj
        context['reviews'] = self.paginator.page(self.page_number)
        if review_list.count() > self.paginate_review:
            context['is_paginated'] = True

        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.variants = ACTIVE_ORDERED_VARIANT_PRODUCTS.filter(
            product=self.object)
        get_dict = request.GET

        if 'variant' in get_dict:
            self.current_variant = self.variants.get(
                id=get_dict['variant'])
        else:
            self.current_variant = self.variants.first()

        self.color_list = self.get_colors_with_href(Color.objects.filter(
            variants__in=self.variants).distinct(), self.request.GET)
        self.size_list = self.get_sizes_with_href(Size.objects.filter(
            variants__in=self.variants).distinct(), self.request.GET)

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            review = review_form.save(commit=False)
            if self.request.user.is_authenticated:
                review.user = self.request.user
            review.product = self.get_object()
            review.save()
            request.POST = {}
            url = request.build_absolute_uri(
                reverse('admin:store_review_change', args=(review.id,)))
            new_review.delay(url)
            messages.success(request, _('Спасибо! Для нас важно наше мнение!'))
        return self.get(request, *args, **kwargs)
