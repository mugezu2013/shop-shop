from django.conf import settings
from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.core.models import Feedback, STATUS_FEEDBACK
from .tasks import new_feedback_for_recipients, feedback_retry


@receiver(post_save, sender=Feedback)
def send_email_for_feedback(sender, instance, **kwargs):
    if instance.status is STATUS_FEEDBACK[0][0]:
        protocol = getattr(settings, "PROTOCOL", "http")
        domain = Site.objects.get_current().domain
        port = getattr(settings, "PORT", "")
        if port:
            assert port.startswith(
                ":"), "The PORT setting must have a preceeding ':'."
        url = "%s://%s%s%s" % (
            protocol, domain, port, instance.get_admin_path())
        new_feedback_for_recipients.delay(url)
    elif instance.status is STATUS_FEEDBACK[1][0]:
        feedback_retry.delay(instance.id)
