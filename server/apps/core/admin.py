from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django import forms
from django.contrib import admin
from modeltranslation.admin import (
    TabbedDjangoJqueryTranslationAdmin,
    TranslationStackedInline
)
from solo.admin import SingletonModelAdmin

from .models import (
    NumberPhone, Email, SocialNetwork, Office, Organization, MoneyCurse, Menu,
    Feedback, RecipientsEmail
)
from ..accounts.selectors import ADMIN_OR_STAFF_USER


class NumberPhoneStackedInline(SortableInlineAdminMixin,
                               admin.StackedInline):
    model = NumberPhone
    extra = 0


class EmailStackedInline(SortableInlineAdminMixin,
                         admin.StackedInline):
    model = Email
    extra = 0


class OfficeStackedInline(SortableInlineAdminMixin,
                          TranslationStackedInline):
    model = Office
    extra = 0


class SocialNetworkStackedInline(SortableInlineAdminMixin,
                                 TranslationStackedInline):
    model = SocialNetwork
    extra = 0


class MoneyCurseStackedInline(admin.StackedInline):
    model = MoneyCurse
    extra = 0


@admin.register(Organization)
class OrganizationAdmin(TabbedDjangoJqueryTranslationAdmin,
                        SingletonModelAdmin):
    inlines = (
        NumberPhoneStackedInline, EmailStackedInline,
        SocialNetworkStackedInline,
        OfficeStackedInline, MoneyCurseStackedInline)


@admin.register(MoneyCurse)
class MoneyCurseAdmin(admin.ModelAdmin):
    list_display = ('uah_rate', 'updated',)


@admin.register(Menu)
class MenuAdmin(SortableAdminMixin, TabbedDjangoJqueryTranslationAdmin):
    list_display = ('name', 'url', 'target', 'position_menu', 'show',)
    list_filter = ('position_menu', 'show')


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'email', 'user', 'status', 'created_at', 'updated_at')
    list_filter = ('user', 'status')


class RecipientsEmailForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RecipientsEmailForm, self).__init__(*args, **kwargs)
        self.fields['users'].queryset = ADMIN_OR_STAFF_USER

    class Meta:
        model = RecipientsEmail
        exclude = ('',)


@admin.register(RecipientsEmail)
class RecipientsEmailAdmin(admin.ModelAdmin):
    list_display = (
        'sample_email', 'get_users',)
    list_filter = ('users', 'sample_email')
    form = RecipientsEmailForm

    def get_queryset(self, request):
        # prefetch = Prefetch('users', ADMIN_OR_STAFF_USER)
        return (super(RecipientsEmailAdmin, self).get_queryset(request).
                prefetch_related('users'))
