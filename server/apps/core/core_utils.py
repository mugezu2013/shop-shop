from .models import Organization
from .templatetags.curse_tags import get_current_currency


def get_USD_money(request, value):
    c = get_current_currency(request)
    if c == 'USD':
        return value
    else:
        uah = Organization.get_solo().money_curse.first().uah_rate
        return format(value / uah, '.2f')
