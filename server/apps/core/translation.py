from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from .models import (Menu, Organization, SocialNetwork, Office)


@register(Menu)
class MenuTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Organization)
class OrganizationTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Office)
class OfficeTranslationOptions(TranslationOptions):
    fields = ('country', 'region', 'city', 'address')


@register(SocialNetwork)
class SocialNetworkTranslationOptions(TranslationOptions):
    fields = ('title', 'alt')
