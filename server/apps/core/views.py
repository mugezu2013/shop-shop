from django.db.models import Prefetch, Q
from django.shortcuts import redirect, render
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView
from django_jinja.views.generic import ListView

from apps.store.models import Product, Category, Brand
from apps.store.selectors import (SALE_PRODUCT_VARIANT,
                                  ACTIVE_ORDERED_VARIANT_PRODUCTS)
from shared.mixins import BreadcrumbMixin


class IndexTemplate(ListView):
    template_name = 'core/main.jinja'
    queryset = Category.objects.active().filter(parent=None)
    extra_context = {
        'title': _('Главная страница')
    }

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IndexTemplate, self).get_context_data(
            object_list=object_list, **kwargs)
        prefetch = Prefetch('variants', SALE_PRODUCT_VARIANT)
        ids = list([obj.id for obj in SALE_PRODUCT_VARIANT])
        products_sale = Product.objects.filter(
            variants__id__in=ids
        ).prefetch_related(prefetch).distinct()
        context['products_sale'] = products_sale
        context['top_products'] = Product.objects.top_product()
        context['top_buy'] = Product.objects.top_product(3)
        brands = Brand.objects.all()
        context['brands'] = brands

        return context


class SetCurrencyAction(TemplateView):

    def get(self, request, *args, **kwargs):
        request.session['currency'] = kwargs['currency']
        return redirect(request.META.get('HTTP_REFERER'))


class Search(BreadcrumbMixin, ListView):
    model = Product
    template_name = 'core/products_search.jinja'

    def get_queryset(self):
        try:
            self.search = self.request.GET['search']
            variant_qs = ACTIVE_ORDERED_VARIANT_PRODUCTS.filter(Q(
                vendor_code__icontains=self.search) | Q(
                product__title__icontains=self.search))
            prefetch = Prefetch('variants', variant_qs)

            qs = Product.objects.filter(
                variants__id__in=variant_qs.values_list
                ('id', flat=True)).prefetch_related(
                prefetch).select_related('brand').order_by(
                '-average_rating').distinct()[:5]
        except KeyError:
            self.search = None
            qs = Product.objects.none()

        return qs

    def get_context_data(self, **kwargs):
        context = super(Search, self).get_context_data(**kwargs)
        context['breadcrumbs'].append(self.build_breadcrumb(
            title=_('Поиск: ') + str(self.search),
            path=self.request.path))
        return context


def page_404(request, exception):
    response = render(request, 'core/404.jinja', )

    response.status_code = 404

    return response


def page_403(request, exception):
    response = render(request, 'core/403.jinja', )

    response.status_code = 403

    return response
