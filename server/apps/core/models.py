from constance import config
from django.conf.global_settings import AUTH_USER_MODEL
from django.core.validators import RegexValidator
from django.db import models
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel

from app.settings.contrib.postie import ADMIN_POSTIE_CHOICES
from shared.utils.validators import validate_image_and_svg_file_extension


class Menu(models.Model):
    TARGET_VALUE = (('_blank', '_blank'), ('_self', '_self'))
    POSITION_VALUE = (('Header', 'Header'), ('Footer', 'Footer'))
    name = models.CharField(max_length=100, verbose_name=_('Имя'))
    url = models.CharField(max_length=254, verbose_name=_('url'))
    target = models.CharField(max_length=254, choices=TARGET_VALUE,
                              default='_self',
                              verbose_name=_('Цель'))
    position_menu = models.CharField(max_length=254, choices=POSITION_VALUE,
                                     verbose_name=_('Позиция меню'))
    show = models.BooleanField(max_length=254, default=True, blank=True,
                               verbose_name=_('Показать'))
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Меню')
        verbose_name_plural = _('Меню')

    def get_absolute_url(self):
        return self.url


class Organization(SingletonModel):
    name = models.CharField(max_length=100, verbose_name=_('Имя'),
                            default='Изменить')
    logo_header = models.FileField(upload_to='organisation/logo_header/%d%m%y',
                                   validators=[
                                       validate_image_and_svg_file_extension],
                                   verbose_name=_('Лого хедера'))

    logo_footer = models.FileField(upload_to='organisation/logo_footer/%d%m%y',
                                   validators=[
                                       validate_image_and_svg_file_extension],
                                   verbose_name=_('Лого футера'))
    favicon = models.FileField(blank=True,
                               upload_to='organisation/favicon/%d%m%y',
                               validators=[
                                   validate_image_and_svg_file_extension],
                               verbose_name=_('Иконка'),
                               default=config.DEFAULT_IMAGE_404, )
    privacy_notice = models.URLField(max_length=254, blank=True,
                                     verbose_name=_('Политика '
                                                    'приватности'))
    conditions_of_use = models.URLField(max_length=254, blank=True,
                                        verbose_name=_('Условия использования'))
    facebook_app_id = models.IntegerField(blank=True, default=0,
                                          verbose_name=_("facebook"))

    class Meta(object):
        ordering = ['-name']
        verbose_name = _('Организация')

    def __str__(self):
        return self.name


class NumberPhone(models.Model):
    organization = models.ForeignKey(Organization,
                                     related_name='phones',
                                     on_delete=models.CASCADE,
                                     null=True)
    main = models.BooleanField(default=False, verbose_name=_('Основной'))
    active = models.BooleanField(default=False, verbose_name=_('Активный'))
    phone = models.CharField(
        max_length=15,
        blank=True,
        unique=True,
        verbose_name=_("Телефон"),
        validators=[RegexValidator(r"[+]?\d{9,15}",
                                   message=_(
                                       'Не соответствует формату телефона'))])

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Номер телефона')
        verbose_name_plural = _('Номеры телефонов')

    def __str__(self):
        return self.phone


class Email(models.Model):
    organization = models.ForeignKey(Organization,
                                     related_name='emails',
                                     on_delete=models.CASCADE,
                                     null=True)
    main = models.BooleanField(default=False, verbose_name=_('Основной'))
    active = models.BooleanField(default=False, verbose_name=_('Активный'))
    email = models.EmailField(unique=True, verbose_name=_("Электронный адрес"))

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Почта')
        verbose_name_plural = _('Почты')

    def __str__(self):
        return self.email


class SocialNetwork(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE,
                                     related_name='socials_network',
                                     null=True)
    main = models.BooleanField(default=False, verbose_name=_('Основной'))
    active = models.BooleanField(default=False, verbose_name=_('Активный'))
    title = models.CharField(max_length=245, verbose_name=_('Заголовок'))
    url = models.URLField(blank=True,
                          verbose_name=_('Ссылка'))
    image = models.FileField(upload_to='social_network/%d%m%y',
                             validators=[validate_image_and_svg_file_extension],
                             verbose_name=_('Изображение'),
                             default=config.DEFAULT_IMAGE_404, )
    alt = models.CharField(blank=True, max_length=255)

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Социальная сеть')
        verbose_name_plural = _('Социальные сети')


class Office(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE,
                                     related_name='offices',
                                     null=True)
    main = models.BooleanField(default=False, verbose_name=_('Основной'))
    active = models.BooleanField(default=False, verbose_name=_('Активный'))
    country = models.CharField(max_length=254, verbose_name=_('Страна'))
    region = models.CharField(max_length=254, verbose_name=_('Регион'))
    city = models.CharField(max_length=254, verbose_name=_('Город'))
    post_index = models.CharField(max_length=254,
                                  verbose_name=_('Почтовый индекс'))
    address = models.CharField(max_length=254, verbose_name=_('Адрес'))
    latitude = models.CharField(max_length=254, verbose_name=_('Ширина'))
    longitude = models.CharField(max_length=254, verbose_name=_('Долгота'))
    show = models.BooleanField(default=False, verbose_name=_('Показывать'))

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['order', ]
        verbose_name = _('Офис')
        verbose_name_plural = _('Офисы')


class MoneyCurse(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE,
                                     related_query_name='money_curse',
                                     related_name='money_curse',
                                     )
    uah_rate = models.DecimalField(max_digits=6, decimal_places=2,
                                   verbose_name=_('Курс валют'))
    updated = models.DateTimeField(auto_now=True, blank=True,
                                   verbose_name=_('Обновлено'))

    class Meta(object):
        ordering = ['-updated', ]
        verbose_name = _('Курс валют')
        verbose_name_plural = _('Курсы валют')


STATUS_FEEDBACK = (('new', 'New'), ('done', 'Done'))


class Feedback(models.Model):
    name = models.CharField(max_length=254, verbose_name=_('Имя'))
    email = models.EmailField(verbose_name=_('Электронный адрес'))
    massage = models.TextField(verbose_name=_('Сообщение'))
    user = models.ForeignKey(AUTH_USER_MODEL, blank=True, null=True,
                             on_delete=models.CASCADE,
                             verbose_name=_('Пользователь'))
    status = models.CharField(max_length=254, choices=STATUS_FEEDBACK,
                              verbose_name=_('Статус'),
                              default=STATUS_FEEDBACK[0][0])
    admin_massage = models.TextField(unique=False, blank=True,
                                     verbose_name=_('Сообщение администратора'))
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Создано'))
    updated_at = models.DateTimeField(auto_now=True, blank=True,
                                      verbose_name=_('Обновлено'))

    class Meta:
        verbose_name = _('Отзыв')
        verbose_name_plural = _('Отзывы')

    def get_admin_path(self):
        return reverse_lazy('admin:core_feedback_change', args=(self.id,))


class RecipientsEmail(models.Model):
    sample_email = models.CharField(max_length=254,
                                    choices=ADMIN_POSTIE_CHOICES, unique=True,
                                    verbose_name=_('Шаблон сообщений'))
    users = models.ManyToManyField(AUTH_USER_MODEL,
                                   verbose_name=_('Пользователи'))

    def get_users(self):
        return list([obj.email for obj in self.users.all()])

    class Meta:
        verbose_name = _('Получатель сообщений')
        verbose_name_plural = _('Получатели сообщений')
