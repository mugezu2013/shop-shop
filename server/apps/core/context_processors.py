from django.db.models import Prefetch

from .forms import SearchProduct
from .models import Organization
from .selectors import ACTIVE_AND_MAIN_NUMBER_ORGANIZATION
from ..store.utils import get_or_create_basket


def data(request):
    number_prefetch = Prefetch('phones', ACTIVE_AND_MAIN_NUMBER_ORGANIZATION)
    org = Organization.objects.all().prefetch_related(number_prefetch).first()
    office = org.offices.filter(main=True, active=True).first()
    org_data = {
        'header_logo': org.logo_header.url,
        'footer_logo': org.logo_footer.url,
        'number_phone': org.phones,
        'email': org.emails.filter(main=True, active=True).first(),
        'social_link': org.socials_network.filter(main=True, active=True),
        'latitude': office.latitude,
        'longitude': office.longitude,
        'url_developer': 'https://webcase.com.ua/',
        'curse': org.money_curse.first().uah_rate,
        'search_form': SearchProduct(),
        'basket_count_products': get_or_create_basket(request).count,
    }
    return org_data
