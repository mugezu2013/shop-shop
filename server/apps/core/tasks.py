from decimal import Decimal

import requests
import xmltodict as xmltodict

from app.celery import app
from app.postie_custom import send_mail_custom
from .models import MoneyCurse, Organization, Feedback
from .selectors import get_recipients_email


@app.task
def get_actual_curse_money():
    url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=usd'
    response = requests.get(url)
    obj = xmltodict.parse(response.content)

    MoneyCurse.objects.create(
        organization=Organization.get_solo(),
        uah_rate=Decimal(obj['exchange']['currency']['rate']))


@app.task
def new_feedback_for_recipients(url_user):
    qs = get_recipients_email('new_feedback')

    send_mail_custom(
        event=qs.sample_email,
        recipients=qs.get_users(),
        context={
            'link': url_user,
        },
    )


@app.task
def feedback_retry(id_feedback):
    feedback = Feedback.objects.get(id=id_feedback)
    send_mail_custom(
        event='feedback_retry',
        recipients=[feedback.email, ],
        context={
            'massage': feedback.massage,
            'admin_massage': feedback.admin_massage,
        },
    )
