from django.urls import path

from apps.core import views

urlpatterns = [
    path('', views.IndexTemplate.as_view(), name='home'),
    path('currency/<str:currency>/', views.SetCurrencyAction.as_view(),
         name='set_currency'),
    path('search/', views.Search.as_view(),
         name='search'),
]
