import jinja2
from django_jinja import library


@library.global_function
@jinja2.pass_context
def build_uri(context, obj):
    return context['request'].build_absolute_uri(obj.get_absolute_url())


