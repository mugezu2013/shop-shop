from typing import Dict, Any
from urllib.parse import urlsplit, urlunsplit

import jinja2
from allauth.socialaccount import providers
from django import template
from django.conf import settings
from django.urls.base import resolve, reverse
from django.urls.exceptions import NoReverseMatch, Resolver404
from django.utils.translation import override, get_language, activate
from django_jinja import library

from apps.core.models import Menu

register = template.Library()


@library.global_function
def numerate_pagination(page, num_pages, neighbor_count=2, static=True,
                        start_end=True):
    numerate_pages = []

    extra_left, extra_right = 0, 0
    """"Экстра переменные отвечают за вывод доп. ссылок с противоположной стороны,
    если со своей стороны не помещаються
    Необходим static=False"""

    if not static:
        er = neighbor_count - (page - 1)
        extra_right = er if er >= 0 else 0
        """Экстра ссылки для правой стороны"""

        el = neighbor_count - (num_pages - page)
        extra_left = el if el >= 0 else 0
        """Экстра ссылки для левой стороны"""

    # Слева от текущей
    if page == 1:
        pass
    elif (page - neighbor_count - extra_left) <= 2:
        """Если вместо '...' можно вставить ссылку на
        первую страницу - делаем это"""

        numerate_pages.extend(list(range(
            1,
            page
        )))
    else:
        """Если ссылка певой страницы не будет отображена
        в пагинации - делаем разрыв '...' """
        if start_end:
            """Если нужны граничные ссылки"""
            numerate_pages.append(1)
        numerate_pages.append(None)
        numerate_pages.extend(list(range(
            page - neighbor_count - extra_left,
            page
        )))

    # Добавляем текущую страницу
    numerate_pages.append(page)

    # Справа от текущей

    if page == num_pages:
        pass
    elif (page + neighbor_count + extra_right) >= num_pages - 1:
        """Если ссылка на последнюю страницу помещаеться -
        - отображаем её"""
        numerate_pages.extend(list(range(
            page + 1,
            num_pages + 1
        )))
    else:
        """Если ссылка на последнюю страницу не помещаеться -
        - делаем разрыв"""
        numerate_pages.extend(list(range(
            page + 1,
            page + neighbor_count + 1 + extra_right,
        )))
        numerate_pages.append(None)
        if start_end:
            """Если нужны граничные ссылки"""
            numerate_pages.append(num_pages)
    return numerate_pages


@library.global_function
def url_replace(request, page):
    """Формирование url """
    query = request.GET.copy()
    if page != 1:
        query["page"] = str(page)
    else:
        try:
            del query["page"]
        except KeyError:
            print('query["page"] Key Error')
    return query.urlencode()


@library.global_function
def url_replace_GET(request, page):
    """Формирование url"""

    query = request.GET.copy()
    if page != 1:
        query["page"] = str(page)
        return f'?{query.urlencode()}'
    else:
        query.pop('page')
        return f'?{query.urlencode()}'


def translate_url_404(request, lang_code):
    """
    Given a URL (absolute or relative), try to get its translated version in
    the `lang_code` language (either by i18n_patterns or by translated regex).
    Return the original URL if no translated version is found.
    """
    url = request.build_absolute_uri()
    request_path = request.path
    parsed = urlsplit(url)
    try:
        match = resolve(parsed.path)
    except Resolver404:
        lang_codes = settings.LANGUAGE_CODES_TRANSLATABLE.keys()
        parts = request_path.split('/')

        # Определяем слаг языка, если основной - его нет
        view_lang_code = lang_code if settings.MODELTRANSLATION_DEFAULT_LANGUAGE != lang_code else None

        # Если первая часть не совпадает с кодом какого либо языка
        # что значит - выбран основной язык
        if not parts[1] in lang_codes:
            # путь текущей локализации
            if get_language() == lang_code:
                url = request_path
            # путь другой локализации
            else:
                url = f'/{view_lang_code}{request_path}'
        else:
            # если у кода есть слаг - не основной язык
            if view_lang_code:
                parts[1] = f'/{view_lang_code}'
            # иначе удаляем слаг языка
            else:
                parts.pop(1)

            # если часnей мало, формируем по другому(обходим особенность join)
            url = '/'.join(parts[1:]) if len(parts) > 2 else f'/{parts[1]}'
    else:
        to_be_reversed = "%s:%s" % (
            match.namespace,
            match.url_name) if match.namespace else match.url_name
        with override(lang_code):
            try:
                url = reverse(to_be_reversed, args=match.args,
                              kwargs=match.kwargs)
            except NoReverseMatch:
                pass
            else:
                url = urlunsplit((parsed.scheme, parsed.netloc, url,
                                  parsed.query, parsed.fragment))
    return url


@library.global_function
@jinja2.contextfunction
def translate_url_with_404(context: Dict[str, Any], language: str) -> str:
    request = context['request']
    return translate_url_404(request, language)


@library.global_function
def take_menu(*, position, active=True):
    return Menu.objects.filter(position_menu=position, show=active)


@library.global_function
def translate_url(request, lang):
    path = request.path
    url_parts = resolve(path)

    url = path
    cur_language = get_language()
    try:
        activate(lang)
        url = reverse(url_parts.view_name, kwargs=url_parts.kwargs)
    finally:
        activate(cur_language)

    return "%s" % url
    # return Menu.objects.filter(position_menu=position, show=active)


@library.global_function
def provider_login_url(request, provider_id, **kwargs):
    provider = providers.registry.by_id(provider_id)
    query = kwargs
    if 'next' not in query:
        next_ = request.GET.get('next')
        if next_:
            query['next'] = next_
    else:
        if not query['next']:
            del query['next']
    return provider.get_login_url(request, **query)
