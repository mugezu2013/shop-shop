from decimal import Decimal

from django_jinja import library

from app.settings import DEFAULT_CURRENCY, CURRENCY


@library.global_function
def get_current_currency(request):
    try:
        return request.session['currency']
    except KeyError:
        request.session['currency'] = DEFAULT_CURRENCY
        return request.session['currency']


@library.global_function
def get_list_currency():
    return CURRENCY


@library.global_function
def get_price(request, curse, money, show_currency=True):
    if type(money) is not Decimal:
        return None
    currency = get_current_currency(request)
    if currency == DEFAULT_CURRENCY:
        pass
    else:
        money = money * curse
    if show_currency:
        return '{} {}'.format(format(money, '.2f'), currency)
    else:
        return format(money, '.2f')
