from django import forms
from django.utils.translation import ugettext_lazy as _

from apps.core.models import Feedback


class SearchProduct(forms.Form):
    search = forms.CharField(max_length=254, label=_("Поиск"), )


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ('name', 'email', 'massage')

    def __init__(self, user=None, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)
        self.user = user
        if user:
            self.initial['name'] = user.first_name
            self.initial['email'] = user.email

    def save(self, commit=True):
        self.instance.user = self.user
        return super(FeedbackForm, self).save(commit)
