from apps.core.models import RecipientsEmail, NumberPhone


def get_recipients_email(event):
    return (
        RecipientsEmail.objects.get(
            sample_email__exact=event)
    )


ACTIVE_AND_MAIN_NUMBER_ORGANIZATION = NumberPhone.objects.filter(active=True,
                                                                 main=True)
