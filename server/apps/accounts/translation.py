from modeltranslation.translator import register, TranslationOptions

from .models import Profile


@register(Profile)
class ArticleTranslationOptions(TranslationOptions):
    pass

