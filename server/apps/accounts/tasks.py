from app.celery import app
from app.postie_custom import send_mail_custom
from apps.core.selectors import get_recipients_email


@app.task
def email_confirm_mail_task(url_success, email, password=None):
    if password:
        send_mail_custom(
            event='сonfirm_email_with_password',
            recipients=[email, ],
            context={
                'email': email,
                'activation_link': url_success,
                'password': password
            },
        )
    else:
        send_mail_custom(
            event='сonfirm_email',
            recipients=[email, ],
            context={
                'email': email,
                'activation_link': url_success,
            },
        )


@app.task
def email_reset_mail_task(to_email, url):
    send_mail_custom(
        event='password_reset',
        recipients=[to_email, ],
        context={
            'password_reset_link': url,
            'user_email': to_email,
        },
    )


@app.task
def social_registration(email, password):
    send_mail_custom(
        event='social_registration',
        recipients=[email, ],
        context={
            'email': email,
            'password': password
        },
    )


@app.task
def new_user_for_recipients(url_user):
    qs = get_recipients_email('new_user_for_recipients')
    # email_list_admin = list([obj.email for obj in ADMIN_OR_STAFF_USER])

    send_mail_custom(
        event=qs.sample_email,
        recipients=qs.get_users(),
        context={
            'link': url_user,
        },
    )
