from allauth.account.views import (LoginView, LogoutView, PasswordResetView,
                                   PasswordResetFromKeyView,
                                   PasswordResetDoneView, SignupView,
                                   AccountInactiveView,
                                   PasswordResetFromKeyDoneView,
                                   ConfirmEmailView,
                                   EmailVerificationSentView,
                                   PasswordChangeView, PasswordSetView)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView, TemplateView
from django_jinja.views.generic import ListView

from shared.mixins import BreadcrumbMixin
from .forms import (ProfileForm, ChangeEmail)
from .models import Profile
from ..store.models import Order


class CustomLoginView(BreadcrumbMixin, LoginView):
    template_name = 'accounts/login.jinja'

    def get_breadcrumbs(self):
        breadcrumbs = super().get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=_('Login')
        ))
        return breadcrumbs


class CustomLogoutView(LogoutView):
    template_name = 'accounts/logout.jinja'


class CustomRegistrationView(BreadcrumbMixin, SignupView):
    template_name = 'accounts/registration.jinja'

    def get_breadcrumbs(self):
        breadcrumbs = super().get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=_('Registration')
        ))
        return breadcrumbs


class CustomRegistrationDoneView(BreadcrumbMixin, TemplateView):
    template_name = 'accounts/msg_page.jinja'
    extra_context = {
        'msg_title': _("Спасибо за регистрацию"),
        'msg_subtitle': _(
            "На Email указанный при регистрации было отправлено письмо "
            "для подтверждения почты ")
    }

    def get_breadcrumbs(self):
        breadcrumbs = super().get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=_('Registration done')
        ))
        return breadcrumbs


class CustomConfirmEmailView(BreadcrumbMixin, ConfirmEmailView):
    template_name = 'accounts/email_confirm.jinja'

    def get_breadcrumbs(self):
        breadcrumbs = super().get_breadcrumbs()
        breadcrumbs.append(self.build_breadcrumb(
            title=_('Confirm email')
        ))
        return breadcrumbs


class CustomEmailVerificationSentView(EmailVerificationSentView):
    template_name = 'accounts/msg_page.jinja'
    extra_context = {
        'msg_title': _("Подтвердите ваш email"),
        'msg_subtitle': _(
            "Мы отправили вам e-mail с подтверждением. Для завершения процесса "
            "регистрации перейдите по указанной ссылке. Если вы не получили "
            "наше сообщение в течение нескольких минут, пожалуйста, свяжитесь"
            " с нами. "
        )
    }


class CustomUserPasswordResetView(PasswordResetView):
    """Обработчик востановления пароля"""
    template_name = 'accounts/password_reset.jinja'
    # success_url = reverse_lazy('account:password_reset_done')
    email_template_name = 'accounts/password_reset_email.jinja'
    # form_class = CustomPasswordResetForm


class CustomUserPasswordResetConfirmView(PasswordResetFromKeyView):
    """Обработчик смены пароля по ссылке восстановления"""
    template_name = 'accounts/password_reset_confirm.jinja'
    # success_url = reverse_lazy('account:account_reset_password_from_key_done')


class CustomUserPasswordResetCompleteView(PasswordResetFromKeyDoneView):
    template_name = 'accounts/msg_page.jinja'
    extra_context = {
        'msg_title': _("Пароль изменен"),
        'msg_subtitle': _("Пароль изменен")
    }


class CustomUserPasswordResetDone(PasswordResetDoneView):
    """Обработчик страницы успешной отправки формы востановления пароля"""
    template_name = 'accounts/msg_page.jinja'
    extra_context = {
        'msg_title': _("Ви заказали востановление пароля"),
        'msg_subtitle': _(
            "На Email была отправлена ссылка для восстановления пароля")
    }


class CustomUserPasswordChangeView(LoginRequiredMixin, PasswordChangeView):
    """Обработчик смены пароля авторизованным пользователем"""
    template_name = 'accounts/password_change_form.jinja'
    success_url = reverse_lazy('account:account_password_change_done')


class CustomUserPasswordChangeDoneView(LoginRequiredMixin, TemplateView):
    """Обработчик страницы успешной смены пароля"""
    template_name = 'accounts/msg_page.jinja'
    extra_context = {
        'msg_title': _("Вы воспользовался формой изменения пароля"),
        'msg_subtitle': _("Ваш пароль успешно изменен")
    }


class CustomUserPasswordSetView(PasswordSetView):
    """Обработчик установки пароля авторизованным пользователем"""
    template_name = 'accounts/password_change_form.jinja'


class CustomUserEmailChangeView(LoginRequiredMixin, FormView):
    template_name = 'accounts/password_change_form.jinja'
    success_url = reverse_lazy('account:profile')
    form_class = ChangeEmail

    def get_form_kwargs(self):
        kwargs = super(CustomUserEmailChangeView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save(self.request)
        return super(CustomUserEmailChangeView, self).form_valid(form)


class CustomUserEmailChangeDoneView(LoginRequiredMixin, TemplateView):
    """Обработчик страницы успешной смены пароля"""
    template_name = 'accounts/msg_page.jinja'
    extra_context = {
        'msg_title': _(
            "Вы воспользовался формой изменения Email-а"),
        'msg_subtitle': _(
            "На указанную почту было отправлено письмо с ссылкой для подтверждения"
        )
    }


class CustomDeActiveUserView(AccountInactiveView):
    template_name = 'accounts/msg_page.jinja'
    extra_context = {
        'msg_title': _("Ваш аккаунт не активный"),
        'msg_subtitle': _("Обратитесь к администратору сайта")
    }


class ProfileDetailView(LoginRequiredMixin, FormView):
    template_name = 'accounts/profile.jinja'
    form_class = ProfileForm
    success_url = reverse_lazy('account:profile')

    def get_queryset(self):
        qs = Profile.objects.select_related('user').get_or_create(
            user=self.request.user)
        return qs

    def get_initial(self):
        # qs = self.get_queryset()
        profile = self.get_queryset()[0]

        data = {'email': profile.user.email,
                'phone': profile.phone,
                'last_name': profile.user.last_name,
                'first_name': profile.user.first_name,
                # 'avatar': profile.avatar,
                }
        return data

    def post(self, request, *args, **kwargs):
        form = self.get_form_class()(request.POST, request.FILES,
                                     instance=request.user)
        if form.is_valid():
            form.save()

            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class UserOrdersListView(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'accounts/orders.jinja'

    def get_queryset(self):
        return (
            Order.objects.filter(user=self.request.user)
                .select_related('pay')
                .prefetch_related(
                'variant_order',
                'variant_order__product_variant',
                'variant_order__product_variant__product',
                'variant_order__product_variant__color',
                'variant_order__product_variant__size'
            )
        )
