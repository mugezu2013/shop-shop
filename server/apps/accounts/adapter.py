from allauth.account.adapter import DefaultAccountAdapter
# from allauth.account.forms import
from allauth.utils import build_absolute_uri
from django.urls import reverse

from apps.store.utils import connect_basket
from shared.utils.base import string_generator
from .tasks import (
    email_confirm_mail_task, new_user_for_recipients, email_reset_mail_task
)


class CustomAccountAdapter(DefaultAccountAdapter):

    def login(self, request, user):
        from apps.store.utils import get_basket_session
        print('SUPER ' * 10)
        basket_session = get_basket_session(request)
        result = super().login(request=request, user=user)
        connect_basket(basket_session, request)
        return result

    def send_confirmation_mail(self, request, emailconfirmation, signup):
        print('SIGNUP' * 4, signup)
        signup = True
        activate_url = self.get_email_confirmation_url(request,
                                                       emailconfirmation)

        user = emailconfirmation.email_address.user

        password = None
        print('PASSWORD ', user.has_usable_password(),
              user.check_password(user.password), user.password)
        if not user.has_usable_password() or not user.check_password(
                user.password):
            password = string_generator()
            user.set_password(password)
            user.save()
            print('PASSWORD ', user.has_usable_password(),
                  user.check_password(user.password), user.password)
        email_confirm_mail_task.delay(activate_url,
                                      emailconfirmation.email_address.email,
                                      password)
        link_user_admin = build_absolute_uri(request,
                                             reverse('admin:auth_user_change',
                                                     args=(user.id,)))
        new_user_for_recipients.delay(link_user_admin)

    def send_mail(self, template_prefix, email, context):
        if template_prefix == 'account/email/password_reset_key':
            email_reset_mail_task.delay(email, context['password_reset_url'])
        else:
            super().send_mail(template_prefix, email, context)

        # email_.delay(event, email, context)

    # def save_user_fast(self, request, user, form, commit=True):
