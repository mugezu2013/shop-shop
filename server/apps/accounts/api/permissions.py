from rest_framework.permissions import BasePermission


class IsAuthor(BasePermission):
    def has_permission(self, request, view):
        qs = view.get_queryset().first()
        if qs:
            return bool(qs.author == request.user)
        else:
            return bool(True)


class IsThisUser(BasePermission):
    def has_permission(self, request, view):
        qs = view.get_queryset().first()
        if qs:
            return bool(qs == request.user)
        else:
            return bool(True)
