from django.contrib.auth.models import User
from rest_auth.views import LoginView
from rest_framework import permissions
from standards.const import VIEW_SCOPES
from standards.drf.views import RetrieveUpdateAPIView, CreateAPIView, \
    StandardAPIViewMixin

from apps.accounts.api.permissions import IsThisUser
from apps.accounts.api.serializers import UserSerializer


class UserDetail(RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated,
                          permissions.IsAdminUser | IsThisUser]

    def get_queryset(self):
        return User.objects.filter(pk=self.kwargs.get('pk'))


class UserCreate(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser]


class CustomizedLoginAPIView(StandardAPIViewMixin, LoginView):
    """
    Api авторизации
    """
    action_name = 'generic'
    scopes = (VIEW_SCOPES.generic,)
    # serializer_class = CustomizedLoginSerializer

    # def post(self, request, *args, **kwargs):
    #     self.request = request
    #     self.serializer = self.get_serializer(data=self.request.data,
    #                                           context={'request': request})
    #     self.serializer.is_valid(raise_exception=True)
    #
    #     user = self.serializer.validated_data['user']
    #
    #     # Если пользователь не активирован - редиректим
    #     if user == ValueError:
    #         return Response(
    #             {'redirect': reverse('auth:user-not-activated')},
    #             status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION
    #         )
    #
    #     # Сохраняем корзину пользователя перед авторизацией
    #     # anon_user_from_req = request.user
    #     anon_cart_manager = CartManager(request)
    #
    #     self.login()
    #
    #     cart_manager = CartManager(request)
    #     # Если корзина анонимного пользователя и авторизованного
    #     # пользователя разная - объединяем их
    #     if anon_cart_manager.cart != cart_manager.cart:
    #         cart_manager.merge(anon_cart_manager)
    #     return self.get_response()
