from django.urls import path

from .views import (UserDetail, UserCreate, CustomizedLoginAPIView, )

urlpatterns = [
    path('user/detail/<int:pk>/', UserDetail.as_view(), name='user'),
    path('user/create/', UserCreate.as_view(), name='user-create'),
    path('user/login/', CustomizedLoginAPIView.as_view(), name='login'),
]
