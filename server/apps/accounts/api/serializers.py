from django.contrib.auth.models import User
from standards.drf.serializers import ModelSerializer

from apps.accounts.models import Profile


class UserProfileSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ['phone', ]


class UserSerializer(ModelSerializer):
    profile = UserProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name',
                  'profile', ]

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        instance = super().create(validated_data)
        Profile.objects.create(self, user=instance, **profile_data)
        return instance

    def update(self, instance, validated_data):
        """проверяем на наличие профиля в юзере"""
        if hasattr(instance, 'profile'):
            profile = instance.profile
        else:
            profile = Profile(user=instance)
        """выбираем данные профиля и удаляем их с валидированых данных"""
        profile_validation_data: dict = validated_data.pop('profile')
        """обновляем данные пользователя и его профиля"""
        instance = super().update(instance, validated_data)
        UserProfileSerializer.update(self, instance=profile,
                                     validated_data=profile_validation_data)
        return instance