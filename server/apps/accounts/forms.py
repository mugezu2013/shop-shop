from allauth.account.adapter import get_adapter
from allauth.account.forms import AddEmailForm
from allauth.account.models import EmailAddress
from allauth.account.utils import setup_user_email, send_email_confirmation
from django import forms
from django.conf import settings
from django.contrib.auth.backends import UserModel
from django.contrib.auth.models import User
from django.core.validators import (RegexValidator, )
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from apps.accounts.models import Profile


class ChangeEmail(AddEmailForm):

    def save(self, request):
        email = super().save(request)
        user = request.user
        user.email = self.cleaned_data["email"]
        user.save()
        EmailAddress.objects.filter(user=request.user).exclude(
            email=email).delete()
        return email


class UserFastRegistrationForm(forms.ModelForm):
    phone = forms.CharField(
        max_length=15,
        label=_("Телефон"),
        required=True,
        validators=[RegexValidator(r"[+]?\d{9,15}",
                                   message=_(
                                       'Не соответствует формату телефона'))]
    )

    create = forms.BooleanField(
        label=_('Create your SHOP SHOP account'),
        help_text=_(
            "By creating an account, you agree to SHOP SHOP's "
            "Conditions of Use and Privacy Notice")
    )

    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'last_name', 'phone'
        )

    def __init__(self, *args, **kwargs):
        super(UserFastRegistrationForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance', None)
        self.fields['email'].required = True
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        if instance:
            # del self.fields['create']
            self.fields['create'].required = False
            self.initial['create'] = False
            self.fields['create'].widget = forms.HiddenInput()
            self.initial['phone'] = instance.profile.phone

    def clean_email(self):
        email = self.cleaned_data['email']
        if self.instance.email == '':
            if email:
                try:
                    user = UserModel.objects.get(email__iexact=email)
                except UserModel.DoesNotExist:
                    return email
                raise forms.ValidationError(
                    _('Эта електронная почта уже используется')
                )
        else:
            return email

    @property
    def clean_data_for_address(self):
        self.cleaned_data.pop('create')
        return self.cleaned_data

    def save_and_send_mail(self, request=None, commit=True):
        adapter = get_adapter(request)
        user = adapter.new_user(request)
        adapter.save_user(request, user, self)

        # TODO: Move into adapter `save_user` ?
        setup_user_email(request, user, [])
        profile = Profile.objects.get(user=user)
        profile.phone = self.cleaned_data['phone']
        profile.save()
        send_email_confirmation(request, user,
                                getattr(settings, "ACCOUNT_SIGNUP"))
        return user


class ProfileForm(forms.ModelForm):
    phone = forms.CharField(
        max_length=13,
        label=_("Телефон"),
        validators=[
            RegexValidator(r"[+]?\d{9,13}",
                           message=_('Неверный формат телефона'))]
    )

    email = forms.EmailField(label='Email',
                             required=False,
                             widget=forms.EmailInput(
                                 attrs={'readonly': 'readonly'}
                             ))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email',)

    def save(self, commit=True):
        cd = self.cleaned_data
        phone = cd['phone']
        first_name = cd['first_name']
        last_name = cd['last_name']
        user = UserModel.objects.get(
            Q(username__iexact=self.instance.email) | Q(
                email__iexact=self.instance.email))

        user.last_name = last_name
        user.first_name = first_name
        profile = user.profile
        profile.phone = phone
        profile.save()
        user.save()
