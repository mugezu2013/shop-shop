from allauth.account.models import EmailAddress
from allauth.socialaccount.admin import SocialAccount
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User
from modeltranslation.admin import (TabbedDjangoJqueryTranslationAdmin,
                                    TranslationStackedInline)

from .models import Profile


# from allauth.socialaccount.models import SocialAccount


@admin.register(Profile)
class ProfileAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ['user', 'code']


class ProfileInline(TranslationStackedInline):
    model = Profile
    extra = 0


class SocialAccountInline(admin.StackedInline):
    model = SocialAccount
    extra = 0


class EmailAddressInline(admin.StackedInline):
    model = EmailAddress
    extra = 1


class MyUserAdmin(admin.ModelAdmin):
    list_display = UserAdmin.list_display
    list_filter = ['is_active', 'is_staff']
    inlines = [ProfileInline, SocialAccountInline, EmailAddressInline]
    form = UserChangeForm


admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
# Register your models here.
