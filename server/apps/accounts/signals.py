from django.contrib.auth import user_logged_in
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Profile


@receiver(post_save, sender=User)
def init_number_order(sender, instance, **kwargs):
    Profile.objects.get_or_create(user=instance)


# @receiver(user_logged_in)
# def login_handler(request, user, **kwargs):
#     print('login', user, request.session.session_key)
