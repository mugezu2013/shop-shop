from django.contrib.auth.models import User
from django.db.models import Q

ADMIN_OR_STAFF_USER = User.objects.filter(
    Q(is_superuser=True) | Q(is_staff=True))
