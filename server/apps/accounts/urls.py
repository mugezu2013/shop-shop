from django.urls import path, re_path, include

from .views import (
    CustomLoginView, CustomLogoutView, CustomRegistrationView,
    CustomUserPasswordResetView, CustomUserPasswordResetDone, CustomDeActiveUserView,
    CustomUserPasswordResetCompleteView, CustomUserPasswordResetConfirmView,
    CustomUserPasswordChangeView, CustomConfirmEmailView,
    CustomEmailVerificationSentView,
    ProfileDetailView, UserOrdersListView, CustomUserPasswordSetView,
    CustomUserEmailChangeView, CustomUserPasswordChangeDoneView, CustomUserEmailChangeDoneView
)

urlpatterns = [
    path("signup/", CustomRegistrationView.as_view(), name="account_signup"),
    path("login/", CustomLoginView.as_view(), name="account_login"),
    path("logout/", CustomLogoutView.as_view(), name="account_logout"),
    path("inactive/", CustomDeActiveUserView.as_view(), name="account_inactive"),
    # E-mail
    # path("email/", views.email, name="account_email"),
    path(
        "confirm-email/",
        CustomEmailVerificationSentView.as_view(),
        name="account_email_verification_sent",
    ),
    re_path(
        r"^confirm-email/(?P<key>[-:\w]+)/$",
        CustomConfirmEmailView.as_view(),
        name="account_confirm_email",
    ),
    path(
        "email/change/",
        CustomUserEmailChangeView.as_view(),
        name="account_change_email",
    ),
    path(
        "email/change/done/",
        CustomUserEmailChangeDoneView.as_view(),
        name="account_password_change_done",
    ),
    # password
    path("password/reset/", CustomUserPasswordResetView.as_view(),
         name="account_reset_password"),
    path(
        "password/reset/done/",
        CustomUserPasswordResetDone.as_view(),
        name="account_reset_password_done",
    ),
    re_path(
        r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$",
        CustomUserPasswordResetConfirmView.as_view(),
        name="account_reset_password_from_key",
    ),
    path(
        "password/reset/key/done/",
        CustomUserPasswordResetCompleteView.as_view(),
        name="account_reset_password_from_key_done",
    ),
    path(
        "password/change/",
        CustomUserPasswordChangeView.as_view(),
        name="account_change_password",
    ),
    path(
        "password/change/done/",
        CustomUserPasswordChangeDoneView.as_view(),
        name="account_password_change_done",
    ),
    path("password/set/", CustomUserPasswordSetView.as_view(),
         name="account_set_password"),

    # profile
    path('profile/', include(([
        path('', ProfileDetailView.as_view(), name='profile'),
        path('orders/', UserOrdersListView.as_view(), name='orders'),

    ])))

]

# urlpatterns = [
#     path('login/', CustomLoginView.as_view(), name='login'),
#     path('logout/', CustomLogoutView.as_view(), name='logout'),
#     path("signup/", CustomRegistrationView.as_view(), name="account_signup"),
#
#     # path('registration/', CustomRegistrationView.as_view(),
#     #      name='registration'),
#     path('registration_done/', CustomRegistrationDoneView.as_view(),
#          name='registration_done'),
#     path('registration_success/<str:code>/<str:email>/',
#          CustomRegistrationSuccessView.as_view(),
#          name='registration_success'),
#     path('password-change/', UserPasswordChangeView.as_view(),
#          name='password_change'),
#     path('password-change-done/', UserPasswordChangeDoneView.as_view(),
#          name='password_change_done'),
#     path('password-reset/', UserPasswordResetView.as_view(),
#          name='password_reset'),
#     path('password-reset/done/', UserPasswordResetDone.as_view(),
#          name='password_reset_done'),
#     path('reset/<uidb64>/<token>/', UserPasswordResetConfirmView.as_view(),
#          name='password_reset_confirm'),
#     path('reset/done/', UserPasswordResetCompleteView.as_view(),
#          name='password_reset_complete'),
#     path('profile/', ProfileDetailView.as_view(),
#          name='profile'),
#
# ]
