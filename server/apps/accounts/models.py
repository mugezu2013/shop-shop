from django.conf.global_settings import AUTH_USER_MODEL

from django.core.validators import RegexValidator
from django.db import models

from django.utils.translation import ugettext_lazy


class Profile(models.Model):
    user = models.OneToOneField(AUTH_USER_MODEL, related_name='profile',
                                related_query_name='profile',
                                default=None, on_delete=models.CASCADE)
    code = models.CharField(max_length=50, blank=True, null=True, default=None)

    phone = models.CharField(
        max_length=15,
        blank=True,
        verbose_name=ugettext_lazy("Телефон"),
        validators=[RegexValidator(r"[+]?\d{9,15}",
                                   message=ugettext_lazy(
                                       'Не соответствует формату телефона'))]
    )

    class Meta:
        verbose_name = ugettext_lazy('Профиль пользователя')
        verbose_name_plural = ugettext_lazy('Профиля')
