from django.urls import path

from apps.addition_page.views import (
    TextPageDetailView, ContactsView, FeedbackSendView, PayAndDelivery, About)

urlpatterns = [
    path('page/<slug:slug>/', TextPageDetailView.as_view(),
         name='page'),
    path('contacts/', ContactsView.as_view(),
         name='contacts'),
    path('feedback/send/', FeedbackSendView.as_view(),
         name='feedback_send'),
    path('about/', About.as_view(),
         name='about'),
    path('pay-and-delivery/', PayAndDelivery.as_view(),
         name='pay_and_delivery'),

]
