from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from apps.addition_page.models import Guarantees, ElementPage, AboutCompany, TextPage


@register(Guarantees)
class GuaranteesTranslationOptions(TranslationOptions):

    fields = ('title', 'content')


@register(ElementPage)
class ElementPageTranslationOptions(TranslationOptions):
    fields = ('alt', 'description')


@register(AboutCompany)
class CompanyTranslationOptions(TranslationOptions):
    fields = ('title', 'content_up', 'content_down')


@register(TextPage)
class TextPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'status')

