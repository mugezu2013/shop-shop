from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView, TemplateView
from django_jinja.views.generic import DetailView, ListView
from seo.mixins.views import ModelInstanceViewSeoMixin

from shared.mixins import BreadcrumbMixin
from .models import (TextPage, Guarantees)
from ..core.forms import FeedbackForm


class TextPageDetailView(BreadcrumbMixin, ModelInstanceViewSeoMixin,
                         DetailView):
    template_name = 'addition_page/page.jinja'
    model = TextPage

    def get_queryset(self):
        return TextPage.objects.published()

    def get_breadcrumbs(self):
        breadcrumbs = super().get_breadcrumbs()
        print(self.object)
        breadcrumbs.append(
            self.build_breadcrumb(
                title=self.object.title,
                path=self.object.get_absolute_url()
            )
        )
        return breadcrumbs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_authenticated or not self.object.only_auth:
            context = self.get_context_data(object=self.object)
            return self.render_to_response(context)
        else:
            raise PermissionDenied(_('Access defined'))


class ContactsView(FormView):
    form_class = FeedbackForm
    template_name = 'addition_page/contact.jinja'
    success_url = reverse_lazy('feedback_send')

    def get_form_kwargs(self):
        kwargs = super(ContactsView, self).get_form_kwargs()
        user = self.request.user
        if user.is_authenticated:
            kwargs['user'] = user
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(ContactsView, self).form_valid(form)


class FeedbackSendView(TemplateView):
    template_name = 'core/msg_page.jinja'
    extra_context = {
        'msg_title': _(
            "Спасибо"),
        'msg_subtitle': _(
            "Мы свяжемся с вами"
        )
    }


class About(TemplateView):
    template_name = 'addition_page/about.jinja'


class PayAndDelivery(ListView):
    template_name = 'addition_page/pay_and_delivery.jinja'
    model = Guarantees
    queryset = Guarantees.objects.all().prefetch_related('elements_page')
