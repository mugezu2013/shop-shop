from ckeditor.fields import RichTextField
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from shared.models import SlugifyMixin
from shared.models.orderable import SortableModel
from shared.models.timestamps import TimestampsMixin
from shared.utils.validators import validate_image_and_svg_file_extension


class Guarantees(SlugifyMixin, TimestampsMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'))
    content = RichTextField(null=True, verbose_name=_('Контент'))

    class Meta(object):
        verbose_name = _('Guarantee')
        verbose_name_plural = _('Guarantees')


class ElementPage(SortableModel, models.Model):
    guarantee = models.ForeignKey(Guarantees, on_delete=models.CASCADE,
                                  verbose_name=_('Guarantee'),
                                  related_name='elements_page')
    image = models.FileField(upload_to='element_page/%d%m%y', blank=True,
                             validators=[
                                 validate_image_and_svg_file_extension],
                             verbose_name=_('image'))

    alt = models.CharField(max_length=255, verbose_name=_('alt'), blank=True,
                           default=None)
    description = models.CharField(max_length=255, blank=True,
                                   verbose_name=_('description'))

    class Meta(object):
        verbose_name = _('Element page')
        verbose_name_plural = _('Element pages')
        ordering = ['sort_order']


class AboutCompany(SlugifyMixin, TimestampsMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'))
    content_up = RichTextField(null=True, verbose_name=_('content_up'))
    content_down = RichTextField(null=True, verbose_name=_('content_down'))

    class Meta(object):
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')


STATUS_PAGE = (
    ('published', _('Published')), ('draft', _('Draft')))


class TextPageManager(models.QuerySet):
    def published(self):
        return self.filter(status=STATUS_PAGE[0][0],
                           publish_at__lte=timezone.localtime(timezone.now()))


class TextPage(SlugifyMixin, TimestampsMixin, models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Название'))
    content = RichTextField(null=True, verbose_name=_('content'))

    image = models.FileField(upload_to='text_page/%d%m%y', blank=True,
                             validators=[
                                 validate_image_and_svg_file_extension],
                             verbose_name=_('image'))
    alt = models.CharField(max_length=255, verbose_name=_('alt'), blank=True,
                           default=None)

    only_auth = models.BooleanField(verbose_name=_('Only_auth'), default=False)

    status = models.CharField(choices=STATUS_PAGE, max_length=20,
                              default=STATUS_PAGE[1][0],
                              verbose_name=_('Статус'))
    publish_at = models.DateTimeField(
        null=True,
        verbose_name=_('Publish at'),
    )

    objects = TextPageManager.as_manager()

    def get_absolute_url(self):
        return reverse('page', kwargs={'slug': self.slug})

    class Meta(object):
        verbose_name = _('Text page')
        verbose_name_plural = _('Text pages')
