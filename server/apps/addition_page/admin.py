from adminsortable2.admin import SortableInlineAdminMixin
from django.contrib import admin
from modeltranslation.admin import (
    TranslationStackedInline, TabbedDjangoJqueryTranslationAdmin)

from shared.admin import TimestampsAdmin
from .models import (
    ElementPage, Guarantees, AboutCompany, TextPage
)


class ElementPageStackedInline(SortableInlineAdminMixin,
                               TranslationStackedInline):
    model = ElementPage
    extra = 0


@admin.register(Guarantees)
class GuaranteesAdmin(TimestampsAdmin, TabbedDjangoJqueryTranslationAdmin):
    list_display = (
        'title', 'slug', 'created_at', 'updated_at')
    prepopulated_fields = {"slug": ("title",)}
    inlines = (ElementPageStackedInline,)


@admin.register(AboutCompany)
class AboutCompanyAdmin(TimestampsAdmin, TabbedDjangoJqueryTranslationAdmin):
    list_display = (
        'title', 'slug', 'created_at', 'updated_at')
    prepopulated_fields = {"slug": ("title",)}


@admin.register(TextPage)
class TextPageAdmin(TimestampsAdmin,
                    TabbedDjangoJqueryTranslationAdmin):
    list_display = (
        'title', 'slug', 'status', 'publish_at', 'only_auth')
    prepopulated_fields = {"slug": ("title",)}
    search_fields = ('title',)
