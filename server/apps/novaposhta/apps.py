from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

__all__ = ("NovaposhtaConfig",)


class NovaposhtaConfig(AppConfig):
    name = "apps.novaposhta"
    verbose_name = _("Novaposhta")
