import django_filters.rest_framework
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from .filters import OrderingSearchFilter
from .models import Warehouse, Settlement, Street
from .serializers import SettlementSerializer, WarehouseSerializer, StreetSerializer
from shared.rest.views import BaseAPIViewMixin
from shared.rest.pagination import LimitOffsetPagination

__all__ = (
    'WarehouseListAPIView',
    'SettlementListAPIView',
    'StreetListAPIView'
)


class WarehouseListAPIView(BaseAPIViewMixin, ListAPIView):
    filter_backends = (
        OrderingSearchFilter,
        django_filters.rest_framework.DjangoFilterBackend
    )
    filter_fields = ['city_ref']
    queryset = Warehouse.objects.all()
    serializer_class = WarehouseSerializer
    search_fields = ['description', 'description_ru']
    pagination_class = LimitOffsetPagination
    permission_classes = (AllowAny, )
    LimitOffsetPagination.default_limit_ = 50


class SettlementListAPIView(BaseAPIViewMixin, ListAPIView):
    filter_backends = (OrderingSearchFilter, )
    queryset = Settlement.objects.all()
    search_fields = ['description', 'description_ru']
    serializer_class = SettlementSerializer
    pagination_class = LimitOffsetPagination
    permission_classes = (AllowAny, )
    LimitOffsetPagination.default_limit_ = 50


class StreetListAPIView(BaseAPIViewMixin, ListAPIView):
    filter_backends = (
        OrderingSearchFilter,
        django_filters.rest_framework.DjangoFilterBackend
    )
    filter_fields = ['city_ref']
    queryset = Street.objects.all()
    search_fields = ['description', 'description_ru']
    serializer_class = StreetSerializer
    pagination_class = LimitOffsetPagination
    permission_classes = (AllowAny, )
    LimitOffsetPagination.default_limit_ = 50
