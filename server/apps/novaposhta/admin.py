from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import Settlement, Preferences, Warehouse, Street


@admin.register(Preferences)
class PreferencesAdmin(SingletonModelAdmin):
    pass


@admin.register(Settlement)
class SettlementAdmin(admin.ModelAdmin):
    search_fields = ['ref', 'description', 'description_ru',
                     'settlement_type_description_ru']
    list_display = [
        'ref',
        'description_ru',
        'settlement_type_description_ru',
        'is_company_pickup',
        'is_company_courier'
    ]
    list_filter = [
        'is_company_pickup',
        'is_company_courier'
    ]


@admin.register(Warehouse)
class WarehouseAdmin(admin.ModelAdmin):
    search_fields = ['ref', 'description', 'description_ru']
    list_display = ['ref', 'description_ru', 'city_ref', 'city_description_ru']


@admin.register(Street)
class StreetAdmin(admin.ModelAdmin):
    search_fields = ['ref', 'description', 'description_ru']
    list_display = ['ref', 'description', 'city_ref', 'streets_type']
