from typing import Dict, Optional

from pydantic import BaseModel, validator


class SettlementSchema(BaseModel):
    ref: str
    settlement_type: str
    description: str
    description_ru: str
    area_ref: str
    settlement_type_description: Optional[str] = ""
    settlement_type_description_ru: Optional[str] = ""
    delivery1: Optional[bool] = False
    delivery2: Optional[bool] = False
    delivery3: Optional[bool] = False
    delivery4: Optional[bool] = False
    delivery5: Optional[bool] = False
    delivery6: Optional[bool] = False
    delivery7: Optional[bool] = False

    @validator(
        'delivery1',
        'delivery2',
        'delivery3',
        'delivery4',
        'delivery5',
        'delivery6',
        'delivery7',
        pre=True
    )
    def nullable_bool(cls, value):
        if value == '':
            return False

        return value


class WarehouseSchema(BaseModel):
    ref: str
    description: str
    description_ru: str
    city_ref: str
    city_description: str
    city_description_ru: str
    site_key: str
    warehouse_type: str
    number: int
    longitude: float
    latitude: float
    phone: str
    reception: Dict
    delivery: Dict
    schedule: Dict


class StreetSchema(BaseModel):
    ref: str
    city_ref: str
    description: str
    description_ru: str = ''
    streets_type_ref: str = ''
    streets_type: str = ''
