from django.core.management.base import BaseCommand

from apps.novaposhta.models import Settlement
from apps.novaposhta.tasks import sync_data


class Command(BaseCommand):
    help = 'Import np'

    def handle(self, *args, **options):
        Settlement.objects.all().delete()
        sync_data()