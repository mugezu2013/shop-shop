from typing import Iterator, List, Dict, Optional
import json

from shared.requester import Requester


class NovaposhtaSDK:
    API_URL = "https://api.novaposhta.ua/v2.0/json/"

    def __init__(self, key: str, requester: Optional[Requester] = None):
        self.key = key
        self.requester = requester or Requester()

    def get_settlements(self) -> Iterator[Dict]:
        body = {
            "modelName": "Address",
            "calledMethod": "getCities",
            "methodProperties": {
                "Page": 1
            },
            "apiKey": self.key
        }

        while True:
            response = self.requester.make_request(
                self.API_URL, 'POST', data=json.dumps(body), headers={
                    "Content-Type": "application/json"
                }
            )

            if not response['data']:
                break

            for instance in response['data']:
                yield instance

            body["methodProperties"]["Page"] += 1

    def get_warehouses(self) -> Iterator[Dict]:
        body = {
            "modelName": "AddressGeneral",
            "calledMethod": "getWarehouses",
            "methodProperties": {
                "Page": 1
            },
            "apiKey": self.key
        }

        response = self.requester.make_request(
            self.API_URL, 'POST', data=json.dumps(body), headers={
                "Content-Type": "application/json"
            }
        )

        for instance in response['data']:
            yield instance

    def get_streets(self, city_ref: str):
        body = {
            "modelName": "Address",
            "calledMethod": "getStreet",
            "methodProperties": {
                "CityRef": city_ref
            },
            "apiKey": self.key
        }

        response = self.requester.make_request(
            self.API_URL, 'POST', data=json.dumps(body), headers={
                "Content-Type": "application/json"
            }
        )

        for instance in response['data']:
            yield instance
