from shared.rest.serializers import BaseModelSerializer

from .models import Warehouse, Settlement, Street
from shared.rest.fields import JSONStringField


class WarehouseSerializer(BaseModelSerializer):
    schedule = JSONStringField()
    reception = JSONStringField()
    delivery = JSONStringField()

    class Meta:
        model = Warehouse
        fields = [
            'id',
            'description_ru',
            'description',
            'city_description',
            'city_description_ru',
            'latitude',
            'longitude',
            'phone',
            'schedule',
            'reception',
            'delivery'
        ]


class SettlementSerializer(BaseModelSerializer):
    class Meta:
        model = Settlement
        fields = [
            'id',
            'description_ru',
            'description',
            'settlement_type_description',
            'settlement_type_description_ru',
            'ref'
        ]


class StreetSerializer(BaseModelSerializer):
    class Meta:
        model = Street
        fields = [
            'id',
            'ref',
            'city_ref',
            'description',
            'description_ru',
            'streets_type',
        ]
