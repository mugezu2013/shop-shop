from django.urls import include, path

from apps.novaposhta.views import SettlementListAPIView, WarehouseListAPIView, StreetListAPIView

app_name = 'novaposhta-api'

urlpatterns = [
    path('novaposhta/', include(([
        path('warehouses/list/', WarehouseListAPIView.as_view(), name='warehouses'),
        path('settlements/list/', SettlementListAPIView.as_view(), name='settlements'),
        path('streets/list/', StreetListAPIView.as_view(), name='streets')
    ])))
]
