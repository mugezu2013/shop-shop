from typing import TYPE_CHECKING, Dict, Type

from apps.novaposhta.schemas import SettlementSchema, WarehouseSchema, StreetSchema
from apps.novaposhta.models import Settlement, Warehouse, Street

if TYPE_CHECKING:
    from apps.novaposhta.sdk import NovaposhtaSDK
    from django.db.models import Model
    from pydantic import BaseModel


SETTLEMENT_FIELDS_MAP = {
    'Ref': 'ref',
    "SettlementType": "settlement_type",
    "Description": "description",
    "DescriptionRu": "description_ru",
    "SettlementTypeDescription": "settlement_type_description",
    "SettlementTypeDescriptionRu": "settlement_type_description_ru",
    "Area": "area_ref",
    "Delivery1": "delivery1",
    "Delivery2": "delivery2",
    "Delivery3": "delivery3",
    "Delivery4": "delivery4",
    "Delivery5": "delivery5",
    "Delivery6": "delivery6",
    "Delivery7": "delivery7",
    "Warehouse": "has_warehouse"
}

WAREHOUSE_FIELDS_MAP = {
    'Ref': 'ref',
    "Description": "description",
    "DescriptionRu": "description_ru",
    "SiteKey": "site_key",
    "TypeOfWarehouse": "warehouse_type",
    "Number": "number",
    "CityRef": "city_ref",
    "CityDescription": "city_description",
    "CityDescriptionRu": "city_description_ru",
    "Longitude": "longitude",
    "Latitude": "latitude",
    "Phone": "phone",
    "Reception": "reception",
    "Delivery": "delivery",
    "Schedule": "schedule",
}

STREET_FIELDS_MAP = {
    'Ref': 'ref',
    "Description": "description",
    "DescriptionRu": "description_ru",
    "StreetsTypeRef": "streets_type_ref",
    "StreetsType": "streets_type",
}


def map_fields(instance: Dict, fields_map: Dict[str, str]):
    return {
        field: value for key, value in instance.items()
        if (field := fields_map.get(key))
    }


def process_settlements(sdk: "NovaposhtaSDK", save_handler: "SaveHandler"):
    db_settlements = Settlement.objects.in_bulk(field_name='ref')

    for settlement in sdk.get_settlements():
        instance = SettlementSchema(**map_fields(settlement, SETTLEMENT_FIELDS_MAP))
        db_instance = db_settlements.get(instance.ref)

        process_instance(db_instance, save_handler, instance, Settlement)

    save_handler.process()


def process_warehouses(sdk: "NovaposhtaSDK", save_handler: "SaveHandler"):
    db_warehouses = Warehouse.objects.in_bulk(field_name='ref')

    for warehouse in sdk.get_warehouses():
        instance = WarehouseSchema(**map_fields(warehouse, WAREHOUSE_FIELDS_MAP))
        db_instance = db_warehouses.get(instance.ref)

        process_instance(db_instance, save_handler, instance, Warehouse)

    save_handler.process()


def process_streets(sdk: "NovaposhtaSDK", save_handler: "SaveHandler"):
    db_streets = Street.objects.in_bulk(field_name='ref')

    for settlement in Settlement.objects.all():
        for street in sdk.get_streets(settlement.werehouse_id):
            fields = map_fields(street, STREET_FIELDS_MAP)
            fields['city_ref'] = settlement.werehouse_id
            instance = StreetSchema(**fields)
            db_instance = db_streets.get(instance.ref)
            process_instance(db_instance, save_handler, instance, Street)

        save_handler.process()


def process_instance(
        db_instance: "Model",
        save_handler: "SaveHandler",
        instance: "BaseModel",
        model: Type['Model']
):
    if not db_instance:
        save_handler.save(model(**instance.dict()))
    else:
        for field, value in instance.dict().items():
            setattr(db_instance, field, value)

        save_handler.save(db_instance)


class SaveHandler:
    ACTIONS = ['create', 'update']
    MAX_INSTANCES = 200

    def __init__(self, model: Type["Model"]):
        self.to_update = []
        self.to_create = []
        self.model = model
        self.fields = [
            x.name for x in model._meta.get_fields() if x.name != 'id'
            if x.concrete
        ]

    def save(self, instance: "Model"):
        if not instance.id:
            self.to_create.append(instance)
        else:
            self.to_update.append(instance)

        for action in self.ACTIONS:
            if len(getattr(self, f'to_{action}', [])) > self.MAX_INSTANCES:
                getattr(self, f'perform_{action}')()

    def perform_create(self):
        self.model.objects.bulk_create(
            self.to_create, batch_size=self.MAX_INSTANCES
        )
        self.to_create = []

    def perform_update(self):
        self.model.objects.bulk_update(
            self.to_update, fields=self.fields, batch_size=self.MAX_INSTANCES
        )
        self.to_update = []

    def process(self):
        for action in self.ACTIONS:
            getattr(self, f'perform_{action}')()
