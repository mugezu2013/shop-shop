first_request_data = [
    {
        "data": [
            {

                "Ref": "e718a680-4b33-11e4-ab6d-005056801329",
                "SettlementType": "563ced10-f210-11e3-8c4a-0050568002cf",
                "Latitude": "50.450418000000000",
                "Longitude": "30.523541000000000",
                "Description": "Київ",
                "DescriptionRu": "Киев",
                "SettlementTypeDescription": "місто",
                "SettlementTypeDescriptionRu": "город",
                "Region": "",
                "RegionsDescription": "",
                "RegionsDescriptionRu": "",
                "Area": "dcaadb64-4b33-11e4-ab6d-005056801329",
                "AreaDescription": "Київська область",
                "AreaDescriptionRu": "Киевская область",
                "Index1": "01001",
                "Index2": "04655",
                "IndexCOATSU1": "3200000000",
                "Delivery1": "1",
                "Delivery2": "1",
                "Delivery3": "1",
                "Delivery4": "1",
                "Delivery5": "1",
                "Delivery6": "1",
                "Delivery7": "0",
                "Warehouse": "1",
                "Conglomerates": [
                    "d4771ed0-4fb7-11e4-91b8-2f592fe1dcac",
                    "f86b75e9-42f4-11e4-91b8-2f592fe1dcac"
                ]
            }
        ]
    },
    {
        "data": [
            {

                "Ref": "e718a680-4b33-11e4-ab6d-005056801328",
                "SettlementType": "563ced10-f210-11e3-8c4a-0050568002cf",
                "Latitude": "50.450418000000000",
                "Longitude": "30.523541000000000",
                "Description": "Київ",
                "DescriptionRu": "Киев",
                "SettlementTypeDescription": "місто",
                "SettlementTypeDescriptionRu": "город",
                "Region": "",
                "RegionsDescription": "",
                "RegionsDescriptionRu": "",
                "Area": "dcaadb64-4b33-11e4-ab6d-005056801329",
                "AreaDescription": "Київська область",
                "AreaDescriptionRu": "Киевская область",
                "Index1": "01001",
                "Index2": "04655",
                "IndexCOATSU1": "3200000000",
                "Delivery1": "1",
                "Delivery2": "1",
                "Delivery3": "1",
                "Delivery4": "1",
                "Delivery5": "1",
                "Delivery6": "1",
                "Delivery7": "0",
                "Warehouse": "1",
                "Conglomerates": [
                    "d4771ed0-4fb7-11e4-91b8-2f592fe1dcac",
                    "f86b75e9-42f4-11e4-91b8-2f592fe1dcac"
                ]
            },
            {

                "Ref": "e718a680-4b33-11e4-ab6d-005056801327",
                "SettlementType": "563ced10-f210-11e3-8c4a-0050568002cf",
                "Latitude": "50.450418000000000",
                "Longitude": "30.523541000000000",
                "Description": "Київ",
                "DescriptionRu": "Киев",
                "SettlementTypeDescription": "місто",
                "SettlementTypeDescriptionRu": "город",
                "Region": "",
                "RegionsDescription": "",
                "RegionsDescriptionRu": "",
                "Area": "dcaadb64-4b33-11e4-ab6d-005056801329",
                "AreaDescription": "Київська область",
                "AreaDescriptionRu": "Киевская область",
                "Index1": "01001",
                "Index2": "04655",
                "IndexCOATSU1": "3200000000",
                "Delivery1": "1",
                "Delivery2": "1",
                "Delivery3": "1",
                "Delivery4": "1",
                "Delivery5": "1",
                "Delivery6": "1",
                "Delivery7": "",
                "Warehouse": "1",
                "Conglomerates": [
                    "d4771ed0-4fb7-11e4-91b8-2f592fe1dcac",
                    "f86b75e9-42f4-11e4-91b8-2f592fe1dcac"
                ]
            },
        ]
    }
]

second_request_data = [
    {
        "data": [
            {

                "Ref": "e718a680-4b33-11e4-ab6d-005056801328",
                "SettlementType": "563ced10-f210-11e3-8c4a-0050568002cf",
                "Latitude": "50.450418000000000",
                "Longitude": "30.523541000000000",
                "Description": "Київ",
                "DescriptionRu": "Киев",
                "SettlementTypeDescription": "місто",
                "SettlementTypeDescriptionRu": "город",
                "Region": "",
                "RegionsDescription": "",
                "RegionsDescriptionRu": "",
                "Area": "dcaadb64-4b33-11e4-ab6d-005056801329",
                "AreaDescription": "Київська область",
                "AreaDescriptionRu": "Киевская область",
                "Index1": "01001",
                "Index2": "04655",
                "IndexCOATSU1": "3200000000",
                "Delivery1": "0",
                "Delivery2": "0",
                "Delivery3": "0",
                "Delivery4": "",
                "Delivery5": "",
                "Delivery6": "",
                "Delivery7": "1",
                "Warehouse": "0",
                "Conglomerates": [
                    "d4771ed0-4fb7-11e4-91b8-2f592fe1dcac",
                    "f86b75e9-42f4-11e4-91b8-2f592fe1dcac"
                ]
            },
            {

                "Ref": "e718a680-4b33-11e4-ab6d-005056801327",
                "SettlementType": "563ced10-f210-11e3-8c4a-0050568002cf",
                "Latitude": "50.450418000000000",
                "Longitude": "30.523541000000000",
                "Description": "Київ",
                "DescriptionRu": "Киев",
                "SettlementTypeDescription": "місто",
                "SettlementTypeDescriptionRu": "город",
                "Region": "",
                "RegionsDescription": "",
                "RegionsDescriptionRu": "",
                "Area": "dcaadb64-4b33-11e4-ab6d-005056801329",
                "AreaDescription": "Київська область",
                "AreaDescriptionRu": "Киевская область",
                "Index1": "01001",
                "Index2": "04655",
                "IndexCOATSU1": "3200000000",
                "Delivery1": "0",
                "Delivery2": "0",
                "Delivery3": "0",
                "Delivery4": "0",
                "Delivery5": "0",
                "Delivery6": "0",
                "Delivery7": "1",
                "Warehouse": "0",
                "Conglomerates": [
                    "d4771ed0-4fb7-11e4-91b8-2f592fe1dcac",
                    "f86b75e9-42f4-11e4-91b8-2f592fe1dcac"
                ]
            },
        ]
    }
]

warehouse_first_request_data = [
    {
        "data": [
            {
                "SiteKey": "10119",
                "Description": "Відділення №1: вул. М. Грушевського, 3",
                "DescriptionRu": "Отделение №1: ул. М. Грушевского, 3",
                "Phone": "0-800-500-609",
                "TypeOfWarehouse": "9a68df70-0267-42a8-bb5c-37f427e36ee4",
                "Ref": "39931b80-e1c2-11e3-8c4a-0050568002cf",
                "Number": "1",
                "CityRef": "20982d74-9b6c-11e2-a57a-d4ae527baec3",
                "CityDescription": "Заболотів (Снятинський р-н)",
                "CityDescriptionRu": "Заболотов (Снятинский р-н)",
                "Longitude": "25.299652300000000",
                "Latitude": "48.470805500000000",
                "PostFinance": "1",
                "BicycleParking": "0",
                "POSTerminal": "1",
                "InternationalShipping": "0",
                "TotalMaxWeightAllowed": 0,
                "PlaceMaxWeightAllowed": 0,
                "Reception": {
                    "Monday": "15:30-18:00",
                    "Tuesday": "15:30-18:00",
                    "Wednesday": "15:30-18:00",
                    "Thursday": "15:30-18:00",
                    "Friday": "15:30-18:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Delivery": {
                    "Monday": "09:00-15:00",
                    "Tuesday": "09:00-15:00",
                    "Wednesday": "09:00-15:00",
                    "Thursday": "09:00-15:00",
                    "Friday": "09:00-15:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Schedule": {
                    "Monday": "09:00-18:00",
                    "Tuesday": "09:00-18:00",
                    "Wednesday": "09:00-18:00",
                    "Thursday": "09:00-18:00",
                    "Friday": "09:00-18:00",
                    "Saturday": "09:00-15:00",
                    "Sunday": "-"
                }
            },
            {
                "SiteKey": "10119",
                "Description": "Відділення №1: вул. М. Грушевського, 3",
                "DescriptionRu": "Отделение №1: ул. М. Грушевского, 3",
                "Phone": "0-800-500-609",
                "TypeOfWarehouse": "9a68df70-0267-42a8-bb5c-37f427e36ee4",
                "Ref": "39931b80-e1c2-11e3-8c4a-0050568002cd",
                "Number": "1",
                "CityRef": "20982d74-9b6c-11e2-a57a-d4ae527baec3",
                "CityDescription": "Заболотів (Снятинський р-н)",
                "CityDescriptionRu": "Заболотов (Снятинский р-н)",
                "Longitude": "25.299652300000000",
                "Latitude": "48.470805500000000",
                "PostFinance": "1",
                "BicycleParking": "0",
                "POSTerminal": "1",
                "InternationalShipping": "0",
                "TotalMaxWeightAllowed": 0,
                "PlaceMaxWeightAllowed": 0,
                "Reception": {
                    "Monday": "15:30-18:00",
                    "Tuesday": "15:30-18:00",
                    "Wednesday": "15:30-18:00",
                    "Thursday": "15:30-18:00",
                    "Friday": "15:30-18:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Delivery": {
                    "Monday": "09:00-15:00",
                    "Tuesday": "09:00-15:00",
                    "Wednesday": "09:00-15:00",
                    "Thursday": "09:00-15:00",
                    "Friday": "09:00-15:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Schedule": {
                    "Monday": "09:00-18:00",
                    "Tuesday": "09:00-18:00",
                    "Wednesday": "09:00-18:00",
                    "Thursday": "09:00-18:00",
                    "Friday": "09:00-18:00",
                    "Saturday": "09:00-15:00",
                    "Sunday": "-"
                }
            },
            {
                "SiteKey": "10119",
                "Description": "Відділення №1: вул. М. Грушевського, 3",
                "DescriptionRu": "Отделение №1: ул. М. Грушевского, 3",
                "Phone": "0-800-500-609",
                "TypeOfWarehouse": "9a68df70-0267-42a8-bb5c-37f427e36ee4",
                "Ref": "39931b80-e1c2-11e3-8c4a-0050568002cv",
                "Number": "1",
                "CityRef": "20982d74-9b6c-11e2-a57a-d4ae527baec3",
                "CityDescription": "Заболотів (Снятинський р-н)",
                "CityDescriptionRu": "Заболотов (Снятинский р-н)",
                "Longitude": "25.299652300000000",
                "Latitude": "48.470805500000000",
                "PostFinance": "1",
                "BicycleParking": "0",
                "POSTerminal": "1",
                "InternationalShipping": "0",
                "TotalMaxWeightAllowed": 0,
                "PlaceMaxWeightAllowed": 0,
                "Reception": {
                    "Monday": "15:30-18:00",
                    "Tuesday": "15:30-18:00",
                    "Wednesday": "15:30-18:00",
                    "Thursday": "15:30-18:00",
                    "Friday": "15:30-18:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Delivery": {
                    "Monday": "09:00-15:00",
                    "Tuesday": "09:00-15:00",
                    "Wednesday": "09:00-15:00",
                    "Thursday": "09:00-15:00",
                    "Friday": "09:00-15:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Schedule": {
                    "Monday": "09:00-18:00",
                    "Tuesday": "09:00-18:00",
                    "Wednesday": "09:00-18:00",
                    "Thursday": "09:00-18:00",
                    "Friday": "09:00-18:00",
                    "Saturday": "09:00-15:00",
                    "Sunday": "-"
                }
            },
        ]
    }
]

warehouse_second_request_data = [
    {
        "data": [
            {
                "SiteKey": "10119",
                "Description": "Відділення №1: вул. М. Грушевського, 3",
                "DescriptionRu": "Отделение №1: ул. М. Грушевского, 3",
                "Phone": "0-800-500-609",
                "TypeOfWarehouse": "9a68df70-0267-42a8-bb5c-37f427e36ee4",
                "Ref": "39931b80-e1c2-11e3-8c4a-0050568002cf",
                "Number": "1",
                "CityRef": "20982d74-9b6c-11e2-a57a-d4ae527baec3",
                "CityDescription": "Заболотів (Снятинський р-н)",
                "CityDescriptionRu": "Заболотов (Снятинский р-н)",
                "Longitude": "25.299652300000000",
                "Latitude": "48.470805500000000",
                "PostFinance": "1",
                "BicycleParking": "0",
                "POSTerminal": "1",
                "InternationalShipping": "0",
                "TotalMaxWeightAllowed": 0,
                "PlaceMaxWeightAllowed": 0,
                "Reception": {
                    "Monday": "15:30-18:00",
                    "Tuesday": "15:30-18:00",
                    "Wednesday": "15:30-18:00",
                    "Thursday": "15:30-18:00",
                    "Friday": "15:30-18:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Delivery": {
                    "Monday": "09:00-15:00",
                    "Tuesday": "09:00-15:00",
                    "Wednesday": "09:00-15:00",
                    "Thursday": "09:00-15:00",
                    "Friday": "09:00-15:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Schedule": {
                    "Monday": "09:00-18:00",
                    "Tuesday": "09:00-18:00",
                    "Wednesday": "09:00-18:00",
                    "Thursday": "09:00-18:00",
                    "Friday": "09:00-18:00",
                    "Saturday": "09:00-15:00",
                    "Sunday": "-"
                }
            },
            {
                "SiteKey": "10119",
                "Description": "Відділення №1: вул. М. Грушевського, 3",
                "DescriptionRu": "Отделение №1: ул. М. Грушевского, 3",
                "Phone": "0-800-500-609",
                "TypeOfWarehouse": "9a68df70-0267-42a8-bb5c-37f427e36ee1",
                "Ref": "39931b80-e1c2-11e3-8c4a-0050568002cd",
                "Number": "2",
                "CityRef": "20982d74-9b6c-11e2-a57a-d4ae527baec3",
                "CityDescription": "Заболотів (Снятинський р-н)",
                "CityDescriptionRu": "Заболотов (Снятинский р-н)",
                "Longitude": "25.299652300000000",
                "Latitude": "48.470805500000000",
                "PostFinance": "1",
                "BicycleParking": "0",
                "POSTerminal": "1",
                "InternationalShipping": "0",
                "TotalMaxWeightAllowed": 0,
                "PlaceMaxWeightAllowed": 0,
                "Reception": {
                    "Monday": "15:30-18:00",
                    "Tuesday": "15:30-18:00",
                    "Wednesday": "15:30-18:00",
                    "Thursday": "15:30-18:00",
                    "Friday": "15:30-18:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Delivery": {
                    "Monday": "09:00-15:00",
                    "Tuesday": "09:00-15:00",
                    "Wednesday": "09:00-15:00",
                    "Thursday": "09:00-15:00",
                    "Friday": "09:00-15:00",
                    "Saturday": "-",
                    "Sunday": "-"
                },
                "Schedule": {
                    "Monday": "09:00-18:00",
                    "Tuesday": "09:00-18:00",
                    "Wednesday": "09:00-18:00",
                    "Thursday": "09:00-18:00",
                    "Friday": "09:00-18:00",
                    "Saturday": "09:00-15:00",
                    "Sunday": "-"
                }
            },
        ]
    }
]