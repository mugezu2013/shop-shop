import json
from typing import List, Dict, Any

import pytest

from apps.novaposhta.importer import (
    SaveHandler, process_settlements,
    process_warehouses,
)
from apps.novaposhta.models import Settlement, Warehouse
from apps.novaposhta.sdk import NovaposhtaSDK
from .fixtures import (
    first_request_data, second_request_data,
    warehouse_first_request_data, warehouse_second_request_data,
)


class MockRequester:
    def __init__(self, response_data: List[Dict[str, Any]]):
        self.response_data = response_data

    def make_request(self, *args, **kwargs) -> Dict[str, Any]:
        page = json.loads(kwargs['data'])['methodProperties']['Page']

        if page > len(self.response_data):
            return {'data': []}

        return self.response_data[page - 1]


@pytest.mark.django_db
def test_process_settlements():
    sdk = NovaposhtaSDK("123", MockRequester(response_data=second_request_data))
    process_settlements(sdk, SaveHandler(Settlement))

    assert Settlement.objects.count() == 2
    obj = Settlement.objects.get(ref="e718a680-4b33-11e4-ab6d-005056801328")
    assert obj.delivery1 is False

    sdk = NovaposhtaSDK("123", MockRequester(response_data=first_request_data))
    process_settlements(sdk, SaveHandler(Settlement))

    assert Settlement.objects.count() == 3
    obj = Settlement.objects.get(ref="e718a680-4b33-11e4-ab6d-005056801328")
    assert obj.delivery1 is True


@pytest.mark.django_db
def test_process_warehouses():
    sdk = NovaposhtaSDK(
        "123", MockRequester(response_data=warehouse_second_request_data)
    )
    process_warehouses(sdk, SaveHandler(Warehouse))

    assert Warehouse.objects.count() == 2
    obj = Warehouse.objects.get(ref="39931b80-e1c2-11e3-8c4a-0050568002cd")
    assert obj.number == 2

    sdk = NovaposhtaSDK(
        "123", MockRequester(response_data=warehouse_first_request_data)
    )
    process_warehouses(sdk, SaveHandler(Warehouse))

    assert Warehouse.objects.count() == 3
    obj = Warehouse.objects.get(ref="39931b80-e1c2-11e3-8c4a-0050568002cd")
    assert obj.number == 1