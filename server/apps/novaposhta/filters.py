import operator
from functools import reduce

from django.db import models
from django.utils import six
from rest_framework.compat import distinct
from rest_framework.filters import SearchFilter

__all__ = (
    'OrderingSearchFilter',
)


class OrderingSearchFilter(SearchFilter):
    def filter_queryset(self, request, queryset, view):
        search_fields = getattr(view, 'search_fields', None)
        search_terms = self.get_search_terms(request)

        if not search_fields or not search_terms:
            return queryset

        orm_lookups = [
            self.construct_search(six.text_type(search_field))
            for search_field in search_fields
        ]

        base = queryset
        conditions = []
        for search_term in search_terms:
            queries = [
                models.Q(**{orm_lookup: search_term})
                for orm_lookup in orm_lookups
            ]
            conditions.append(reduce(operator.or_, queries))
        queryset = queryset.filter(reduce(operator.and_, conditions))

        if self.must_call_distinct(queryset, search_fields):
            # Filtering against a many-to-many field requires us to
            # call queryset.distinct() in order to avoid duplicate items
            # in the resulting queryset.
            # We try to avoid this if possible, for performance reasons.
            queryset = distinct(queryset, base)

        search_terms = ' '.join(search_terms)
        queryset = (
            queryset.annotate(
                # set priority to order by
                priority=models.Case(
                    models.When(
                        models.Q(description__iexact=search_terms)
                        | models.Q(description_ru__iexact=search_terms),
                        then=models.Value(1)
                    ),
                    models.When(
                        models.Q(description__istartswith=search_terms)
                        | models.Q(description_ru__istartswith=search_terms),
                        then=models.Value(2)
                    ),
                    default=models.Value(3),
                    output_field=models.SmallIntegerField()
                )
            )
                .order_by('priority')
        )

        return queryset
