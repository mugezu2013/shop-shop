from app.celery import app
from apps.novaposhta.importer import (
    process_settlements,
    SaveHandler,
    process_warehouses,
    process_streets
)
from apps.novaposhta.models import Preferences, Settlement, Warehouse, Street
from apps.novaposhta.sdk import NovaposhtaSDK


@app.task
def sync_data():
    preferences = Preferences.get_solo()
    sdk = NovaposhtaSDK(preferences.key)

    save_handler = SaveHandler(Settlement)
    process_settlements(sdk, save_handler)

    save_handler = SaveHandler(Warehouse)
    process_warehouses(sdk, save_handler)

    save_handler = SaveHandler(Street)
    process_streets(sdk, save_handler)
