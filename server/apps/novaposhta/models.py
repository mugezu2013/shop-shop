from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import gettext_lazy as _
from solo.models import SingletonModel


class Preferences(SingletonModel):
    key = models.CharField(
        verbose_name=_('Ключ доступа'), max_length=125, blank=True,
        null=True
    )

    class Meta:
        verbose_name = _('Настройки новой почты')


class Settlement(models.Model):
    ref = models.CharField(
        verbose_name=_('City ref'), max_length=37, unique=True
    )
    settlement_type = models.CharField(
        verbose_name=_('Тип населенного пункта (Село, ПГТ и т.д.)'),
        max_length=37
    )
    description = models.CharField(
        verbose_name=_('Адрес на Украинском языке'), max_length=51
    )
    description_ru = models.CharField(
        verbose_name=_('Адрес на русском языке'), max_length=51
    )
    area_ref = models.CharField(
        verbose_name=_('Обозначение области'), max_length=37
    )
    settlement_type_description = models.CharField(
        verbose_name=_('Тип населенного пункта'), max_length=37,
        null=True
    )
    settlement_type_description_ru = models.CharField(
        verbose_name=_('Тип населенного пункта на русском'), max_length=37,
        null=True
    )

    delivery1 = models.BooleanField(
        verbose_name=_('Есть доставка 1 день недели'), default=False
    )
    delivery2 = models.BooleanField(
        verbose_name=_('Есть доставка 2 день недели'), default=False
    )
    delivery3 = models.BooleanField(
        verbose_name=_('Есть доставка 3 день недели'), default=False
    )
    delivery4 = models.BooleanField(
        verbose_name=_('Есть доставка 4 день недели'), default=False
    )
    delivery5 = models.BooleanField(
        verbose_name=_('Есть доставка 5 день недели'), default=False
    )
    delivery6 = models.BooleanField(
        verbose_name=_('Есть доставка 6 день недели'), default=False
    )
    delivery7 = models.BooleanField(
        verbose_name=_('Есть доставка 7 день недели'), default=False
    )
    is_company_pickup = models.BooleanField(
        _('Is company pickup enabled'),
        default=False
    )
    is_company_courier = models.BooleanField(
        _('Is company courier enabled'),
        default=False
    )

    class Meta:
        verbose_name = _('Населенный пункт')
        verbose_name_plural = _('Населенный пункты')

    def __str__(self):
        return self.description_ru or self.description


@staticmethod
def autocomplete_search_fields():
    return (
        'ref',
        'description',
        'description_ru',
        'settlement_type_description',
        'settlement_type_description_ru',
    )


class Warehouse(models.Model):
    ref = models.CharField(
        verbose_name=_('Warehouse ref'), max_length=37, unique=True
    )
    site_key = models.IntegerField(verbose_name=_('Site key'))
    description = models.CharField(
        verbose_name=_('Адрес на Украинском языке'), max_length=151
    )
    description_ru = models.CharField(
        verbose_name=_('Адрес на русском языке'), max_length=151
    )
    warehouse_type = models.CharField(
        verbose_name=_('Тип отделения'), max_length=37
    )
    number = models.IntegerField(verbose_name=_('Номер отделения'))
    city_ref = models.CharField(
        verbose_name=_('Идентификатор населенного пункта'), max_length=37,
        db_index=True
    )
    city_description = models.CharField(
        verbose_name=_('Название населенного пункта на Украинском'),
        max_length=151
    )
    city_description_ru = models.CharField(
        verbose_name=_('Название населенного пункта на русском'), max_length=151
    )
    latitude = models.DecimalField(
        max_length=_('Latitude'),
        max_digits=15,
        decimal_places=13
    )
    longitude = models.DecimalField(
        verbose_name=_('Longitude'),
        max_digits=16,
        decimal_places=13
    )
    phone = models.CharField(
        verbose_name=_('Телефон'),
        max_length=120,
        blank=True
    )
    reception = JSONField(verbose_name=_("График приема отправлений"),
                          blank=True, default=dict)
    delivery = JSONField(verbose_name=_("График отправки день в день"),
                         blank=True, default=dict)
    schedule = JSONField(verbose_name=_("График работы"), blank=True,
                         default=dict)

    class Meta:
        verbose_name = _('Отделение')
        verbose_name_plural = _('Отделения')

    def __str__(self) -> str:
        return self.description_ru

    @staticmethod
    def autocomplete_search_fields():
        return (
            'ref',
            'description',
            'description_ru',
        )


class Street(models.Model):
    ref = models.CharField(
        verbose_name=_('Warehouse ref'),
        max_length=37,
        unique=True
    )
    description = models.CharField(
        verbose_name=_('Адрес на Украинском языке'),
        max_length=151
    )
    description_ru = models.CharField(
        verbose_name=_('Адрес на русском языке'),
        blank=True,
        max_length=151
    )
    city_ref = models.CharField(
        verbose_name=_('Идентификатор населенного пункта'), max_length=37,
        db_index=True
    )
    streets_type_ref = models.CharField(
        _("Streets type ref"),
        blank=True,
        max_length=50
    )
    streets_type = models.CharField(
        _("Streets type"),
        blank=True,
        max_length=50
    )

    class Meta:
        verbose_name = _("Street")
        verbose_name_plural = _("Streets")

    @staticmethod
    def autocomplete_search_fields():
        return (
            'ref',
            'description',
            'description_ru',
        )

    def __str__(self) -> str:
        return self.description_ru or self.description
