from django.conf import settings
from django.utils.functional import cached_property
from django.utils.translation import ugettext as _

__all__ = (
    'BreadcrumbMixin',
)


class BreadcrumbMixin:
    home_path = settings.HOME_URL

    @cached_property
    def home_label(self):
        return _('Home')

    def get_breadcrumbs(self):
        return [self.build_breadcrumb(
            path=self.home_path,
            title=self.home_label
        )]

    @staticmethod
    def build_breadcrumb(title: str, path: str = None):
        return {
            'href': path,
            'title': title
        }

    def get_context_data(self, **kwargs):
        kwargs['breadcrumbs'] = self.get_breadcrumbs()
        return super().get_context_data(**kwargs)
