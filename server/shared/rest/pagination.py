from collections import OrderedDict

from rest_framework.pagination import (
    PageNumberPagination as RestPageNumberPagination,
    LimitOffsetPagination as RestLimitOffsetPagination,
    _positive_int
)
from rest_framework.response import Response


class PageNumberPagination(RestPageNumberPagination):
    page_size = 12
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data):
        return Response(
            OrderedDict([
                ('pagination', OrderedDict([
                    ('count', self.page.paginator.count),
                    ('page', self.page.number),
                    ('perPage', self.get_page_size(self.request)),
                    ('hasNext', self.page.has_next()),
                    ('hasPrevious', self.page.has_previous()),
                ])),
                ('items', data)
            ])
        )

    def get_results(self, data):
        return data['data']


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 5000


class LimitOffsetPagination(RestLimitOffsetPagination):
    default_limit_ = 12
    max_limit = 1000

    def get_limit(self, request):
        if self.limit_query_param:
            try:
                return _positive_int(
                    request.query_params[self.limit_query_param],
                    strict=True,
                    cutoff=self.max_limit
                )
            except (KeyError, ValueError) as e:
                pass

        return self.default_limit_

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('pagination', OrderedDict([
                ('total', self.count),
                ('limit', self.get_limit(self.request)),
                ('offset', self.get_offset(self.request)),
                ('next', self.get_next_link()),
                ('previous', self.get_previous_link()),
            ])),
            ('items', data)
        ]))

    def get_results(self, data):
        return data['data']
