import os
import json


from django.contrib.contenttypes.models import ContentType
from django.conf import settings

from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _

import requests
from rest_framework import serializers
from rest_framework.fields import get_attribute
from versatileimagefield.serializers import VersatileImageFieldSerializer

__all__ = (
    'ReCaptchaField',
    'JSONStringField',
    'ChoiceField',
    'DisplayField',
    'GenericRelationField',
    'StringObjectRelatedField',
    'RecursiveField',
    'ArrayChoiceField',
    'ContentTypeNaturalKeyField',
    'WebPVersatileImageFieldSerializer',
    'IgnoreLinkImageField'
)


class ReCaptchaField(serializers.CharField):
    extra_error_messages = {
        'write_only': _('This is a write only field'),
        # 'missing-input-secret': _('Secret key is missing. Please contact admin.'),
        'invalid-input-secret': _('The secret key is invalid or malformed.'),
        'missing-input-response': _('Please check the checkbox to prove you are not a robot.'),
        'invalid-input-response': _('The response is invalid or malformed.'),
        'bad-request': _('Bad request.'),
        'timeout-or-duplicate': _('Verification has timed out or duplicate.')
    }

    default_error_messages = serializers.CharField.default_error_messages.copy()
    default_error_messages.update(extra_error_messages)

    def __init__(self, private_key: str = '', **kwargs):
        self.private_key = private_key

        super().__init__(**kwargs)

    # def to_representation(self, value):
    #     self.fail('write_only')

    def to_internal_value(self, data):
        if os.environ.get('RECAPTCHA_DISABLE', None) is not None:
            return data

        data = super().to_internal_value(data)
        url = 'https://www.google.com/recaptcha/api/siteverify'
        private_key = self.private_key or getattr(settings, 'RECAPTCHA_PRIVATE_KEY', '')

        try:
            res = requests.post(
                url,
                {
                    'secret': private_key,
                    'response': data
                },
                timeout=5
            )
            res.raise_for_status()
        except requests.RequestException as e:
            raise serializers.ValidationError(
                _('Connection to reCaptcha server failed')
            )

        return_values = json.loads(res.content.decode())
        return_code = return_values.get("success", False)
        error_codes = return_values.get('error-codes', [])

        if not return_code:
            if error_codes and error_codes[0] in self.default_error_messages.keys():
                self.fail(error_codes[0])
            else:
                self.fail('bad-request')

        return data


class JSONStringField(serializers.JSONField):
    def to_representation(self, value):
        if not value:
            return value

        try:
            return json.loads(value)
        except TypeError as e:
            return value


class ChoiceField(serializers.DictField):
    """
    Serializer for the choice field functionality.
    """

    def __init__(self, *args, **kwargs):
        self.passed_source = kwargs.get('source', None) or None
        kwargs['source'] = '*'
        super(ChoiceField, self).__init__(*args, **kwargs)

    def to_representation(self, value):
        field = self.passed_source or self.field_name

        return {
            'value': get_attribute(value, [field]),
            'display': getattr(value, f'get_{field}_display')()
        }


class DisplayField(serializers.CharField):
    """
    Field for the `display` functionality.

    In most cases ui like select is used to map to a value-display pair.
    This serializer is for a display field. It just converts object to
    a string representation.
    """

    def __init__(self, *args, **kwargs):
        source = kwargs.get('source', None)
        if source is None:
            kwargs['source'] = '*'

        super(DisplayField, self).__init__(*args, **kwargs)

    def to_representation(self, value):
        return str(value)


class GenericRelationField(serializers.DictField):
    """
    Serialize generic relation objects into simple type|id dicts.
    """

    def to_representation(self, value):
        relation = value._meta.get_field(self.field_name)
        id_ = getattr(value, relation.fk_field, None)

        if not id_:
            return None

        return {
            'type': getattr(value, f'{relation.ct_field}_id', None),
            'id': id_,
            'url': (
                relation.get_absolute_url()
                if hasattr(relation, 'get_absolute_url') else ''
            )
        }


class StringObjectRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        return str(value)


class RecursiveField(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class ArrayChoiceField(serializers.DictField):
    """
    Render choices for array field
    """
    def __init__(self, *args, **kwargs):
        self.choices = kwargs.pop('choices', None) or None
        super().__init__(*args, **kwargs)

    def to_representation(self, value):
        return {
            'value': value,
            'display': self.choices[value]
        }


class ContentTypeNaturalKeyField(serializers.CharField):
    default_error_messages = {
        'invalid': _('Must be a valid natural key format: `app_label.model`. Your value: {value}'),
        'not_exist': _('Content type for natural key `{value}` does not exist.')
    }

    def to_internal_value(self, data):
        try:
            app_label, model = data.split('.')
        except ValueError:
            self.fail('invalid', value=data)
        try:
            ct = ContentType.objects.get_by_natural_key(app_label, model)
        except ContentType.DoesNotExist:
            self.fail('not_exist', value=data)
        return ct


class WebPVersatileImageFieldSerializer(VersatileImageFieldSerializer):
    def to_representation(self, value):
        try:
            value = super().to_representation(value)
        except:
            return None

        for key, image_url in value.items():
            if key.endswith('webp'):
                name, ext = image_url.rsplit('.', 1)
                value[key] = f'{name}.webp'

        return value


class IgnoreLinkImageField(serializers.ImageField):
    def run_validation(self, data=serializers.empty):
        if isinstance(data, str):
            data = serializers.empty
        return super().run_validation(data)


def PhoneField(
    validators: list = None,
    required: bool = True,
    **kwargs
):
    validators = validators if validators is not None else []
    validators.append(RegexValidator(
        regex=r'^\+?3?8?(0\d{9})$', message=_('Phone has an invalid format.')
    ))

    return serializers.CharField(
        min_length=13, max_length=13,
        validators=validators, required=required,
        **kwargs
    )
