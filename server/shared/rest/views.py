from typing import Dict, List

from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse

from rest_framework import status, generics, viewsets
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.generics import (
    GenericAPIView,
    get_object_or_404,
    RetrieveUpdateAPIView as RestRetrieveUpdateAPIView
)
from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response

from shared.rest.mixins import AcceptLanguageMixin


class BaseAPIViewMixin(AcceptLanguageMixin):
    single_message = True
    messages = None
    redirect_url = None
    additional_response = {}
    _finalize_response = True

    def get_messages(self, serializer=None):
        messages: List[Dict] = self.messages

        if messages:
            for message in messages:
                message.setdefault('type', 'success')

        return messages

    def set_messages(self, data: Dict, serializer=None):
        messages = self.get_messages(serializer)

        if messages:
            if self.single_message:
                data['message'] = messages[0]
            else:
                data['messages'] = messages

    def get_redirect_url(self, serializer=None):
        return self.redirect_url

    def set_redirect_url(self, data: Dict, serializer=None):
        redirect_url = self.get_redirect_url(serializer)

        if redirect_url:
            data['redirect'] = {
                'location': redirect_url
            }

    def _get_response_data(self, response: 'Response'):
        data = response.data

        if data is None:
            data = {}

        return data

    def get_response_data(self, response: 'Response', serializer=None):
        data = self._get_response_data(response)

        if data is not None:
            self.set_redirect_url(data, serializer)
            self.set_messages(data, serializer)

        return {
            'code': response.status_code,
            'data': data,
        }

    def finalize_response(self, request, response: 'Response', *args, **kwargs):
        if response.status_code < 400:
            if self._finalize_response:
                response.data = self.get_response_data(response)

        return super().finalize_response(request, response, *args, **kwargs)


class CreateAPIView(BaseAPIViewMixin, generics.CreateAPIView):
    """
    Create Api View with success message
    """
    def _get_response_data(self, response: 'Response'):
        data = super()._get_response_data(response)

        data = {'item': data}

        return data


class ModelViewSet(BaseAPIViewMixin, viewsets.ModelViewSet):
    def _get_response_data(self, response: 'Response'):
        data = super()._get_response_data(response)

        if self.action == 'create':
            data = {'item': data}

        return data


class ListResponseUpdateAPIView(BaseAPIViewMixin, generics.UpdateAPIView):
    """
    Update API View which return list response
    """
    response_serializer_class = None

    def get_response_serializer_class(self):
        return self.response_serializer_class

    def get_response_serializer(self, *args, **kwargs):
        response_serializer_class = self.get_response_serializer_class()
        if response_serializer_class:
            serializer_class = response_serializer_class
            kwargs['context'] = self.get_serializer_context()
            return serializer_class(*args, **kwargs)
        return self.get_serializer(*args, **kwargs)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return (
            Response(
                self.get_response_serializer(
                    self.get_queryset(), many=True, context={'request': request}
                ).data
            )
        )


class ExcelExportListAPIView(BaseAPIViewMixin, generics.ListAPIView):
    """
    List API View with possibility to export queryset to excel
    """
    resource_class = None
    export_file_name = 'default'
    paginate_export_data = True

    def get_resource_class(self):
        if not self.resource_class:
            raise ImproperlyConfigured(
                f'You have to set `resource_class` to {self.__class__.__name__}'
            )
        return self.resource_class

    def get_resource(self):
        resource_class = self.get_resource_class()
        if resource_class:
            return resource_class()

    def get_export_file_name(self):
        return self.export_file_name

    def get_export_queryset(self):
        return self.filter_queryset(self.get_queryset())

    def excel_response(self, request, *args, **kwargs):
        resource = self.get_resource()
        if resource:
            queryset = self.get_export_queryset()
            if self.paginate_export_data:
                page = self.paginate_queryset(queryset)
                if page:
                    queryset = page
            dataset = resource.export(
                queryset=queryset,
                request=self.request,
                view_kwargs=self.kwargs
            )
            resp = HttpResponse(
                dataset.xls,
                content_type='application/vnd.ms-excel'
            )
            resp['Content-Disposition'] = (
                f'attachment; filename="{self.get_export_file_name()}.xls"'
            )
            return resp

    def list(self, request, *args, **kwargs):
        if request.GET.get('type') == 'excel':
            return self.excel_response(request, *args, **kwargs)
        return super().list(request, *args, **kwargs)


class CsvExportListAPIView(BaseAPIViewMixin, generics.ListAPIView):
    """
    List API View with possibility to export queryset to csv file
    """
    converter_class = None
    export_file_name = 'default'

    def get_converter_class(self):
        return self.converter_class

    def get_resource(self):
        resource_class = self.get_converter_class()
        if resource_class:
            return resource_class()

    def get_export_file_name(self):
        return self.export_file_name

    def export_to_csv(self, request, *args, **kwargs):
        resource = self.get_resource()
        if resource:
            resp = HttpResponse(content_type='text/csv')
            resp['Content-Disposition'] = (
                f'attachment; filename="{self.get_export_file_name()}.csv"'
            )
            queryset = self.filter_queryset(self.get_queryset())
            page = self.paginate_queryset(queryset)
            if page is not None:
                queryset = page
            resource.export(
                response=resp,
                queryset=queryset
            )
            return resp

    def list(self, request, *args, **kwargs):
        if request.GET.get('export') == 'csv':
            return self.export_to_csv(request, *args, **kwargs)
        return super().list(request, *args, **kwargs)


class BulkCreateModelMixin(CreateModelMixin):
    def create(self, request, *args, **kwargs):
        bulk = isinstance(request.data, list)

        if not bulk:
            return super(BulkCreateModelMixin, self).create(request, *args, **kwargs)
        else:
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            self.perform_bulk_create(serializer)
            return Response(serializer.data, status=status.HTTP_200_OK)

    def perform_bulk_create(self, serializer):
        return self.perform_create(serializer)


class BulkUpdateModelMixin(object):
    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        if lookup_url_kwarg in self.kwargs:
            return super(BulkUpdateModelMixin, self).get_object()

        # If the lookup_url_kwarg is not present
        # get_object() is most likely called as part of options()
        # which by default simply checks for object permissions
        # and raises permission denied if necessary.
        # Here we don't need to check for general permissions
        # and can simply return None since general permissions
        # are checked in initial() which always gets executed
        # before any of the API actions (e.g. create, update, etc)
        return

    def bulk_update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)

        # restrict the update to the filtered queryset
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()),
            data=request.data,
            many=True,
            partial=partial,
        )

        if serializer.is_valid():
            self.perform_bulk_update(serializer)
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(data={
            'errors': serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)

    def partial_bulk_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.bulk_update(request, *args, **kwargs)

    def perform_bulk_update(self, serializer):
        return self.perform_update(serializer)


class BulkCreateAPIView(
    BulkCreateModelMixin,
    GenericAPIView
):
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class BulkUpdateAPIView(
    BulkUpdateModelMixin,
    GenericAPIView
):
    def put(self, request, *args, **kwargs):
        return self.bulk_update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_bulk_update(request, *args, **kwargs)


class DynamicFieldsAPIViewMixin:
    fields = None
    exclude_fields = None

    def get_serializer_fields(self):
        return self.fields

    def get_serializer_exclude_fields(self):
        return self.exclude_fields

    def get_serializer(self, *args, **kwargs):
        get_params = self.request.GET.copy()
        kwargs['fields'] = (
            self.get_serializer_fields() or
            get_params.getlist('fields')
        )
        kwargs['exclude_fields'] = (
            self.get_serializer_exclude_fields() or
            get_params.getlist('exclude')
        )
        return super().get_serializer(*args, **kwargs)


class OptionsMixin:
    def get_options_response(self, request, *args, **kwargs):
        return {}

    def options(self, request, *args, **kwargs):
        response = super().options(request, *args, **kwargs)
        response.data.update(
            self.get_options_response(request, *args, **kwargs)
        )
        return response


class GetObjectMixin:
    lookup_field = 'pk'
    lookup_url_kwarg = None
    queryset = None

    def get_queryset(self):
        return self.queryset

    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(self.get_queryset(), **filter_kwargs)

        return obj


class RetrieveUpdateAPIView(RestRetrieveUpdateAPIView):
    update_serializer_class = None
    update_permission_classes = None

    def is_get_method(self):
        return self.request.method == 'GET'

    def get_update_serializer_class(self):
        return self.update_serializer_class or self.serializer_class

    def get_serializer_class(self):
        if self.is_get_method():
            return self.serializer_class

        return self.get_update_serializer_class()

    def get_permissions(self):
        if self.is_get_method():
            return super().get_permissions()

        update_permission_classes = (
            self.update_permission_classes or self.permission_classes
        )

        return [permission() for permission in update_permission_classes]


class ActionGenericAPIView(GenericAPIView):

    def perform_action(self, serializer):
        raise NotImplementedError

    def get_response(self, result, serializer):
        return serializer.data

    def post(self, request, *args, **kwargs):
        self.serializer = self.get_serializer(data=request.data)
        self.serializer.is_valid(raise_exception=True)

        result = self.perform_action(self.serializer)

        return Response(
            data=self.get_response(result, self.serializer),
            status=status.HTTP_200_OK
        )


class MethodSerializerView(object):
    """
    Utility class for get different serializer class by method.
    For example:
    method_serializer_classes = {
        ('GET', ): MyModelListViewSerializer,
        ('PUT', 'PATCH'): MyModelCreateUpdateSerializer
    }
    """
    method_serializer_classes = None

    def get_serializer_class(self):
        assert self.method_serializer_classes is not None, (
            'Expected view %s should contain method_serializer_classes '
            'to get right serializer class.' %
            (self.__class__.__name__, )
        )
        for methods, serializer_cls in self.method_serializer_classes.items():
            if self.request.method in methods:
                return serializer_cls

        raise MethodNotAllowed(self.request.method)
