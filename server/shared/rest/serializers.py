import inspect
from typing import TYPE_CHECKING

from django.core.exceptions import ObjectDoesNotExist
from django.db.transaction import atomic
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from .fields import ContentTypeNaturalKeyField

if TYPE_CHECKING:
    from django.contrib.contenttypes.models import ContentType

__all__ = (
    'RequestSerializerMixin',
    'EntitySerializer',
    'BaseModelSerializer',
    'DynamicFieldsSerializerMixin',
    'BulkSerializerMixin',
    'BulkListSerializer',
    'ContentTypeSerializer'
)


class RequestSerializerMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.view = self.context.get('view')
        self.request = self.context.get('request')
        self.user = getattr(self.request, 'user', None)


class EntitySerializer(serializers.ModelSerializer):
    def get_instance_caption(self, instance):
        return str(instance)

    def to_representation(self, instance):
        props = super().to_representation(instance)
        return {
            'id': instance.pk,
            'caption': self.get_instance_caption(instance),
            'type': (
                instance._type if hasattr(instance, '_type')
                else instance.__class__.__name__.lower()
            ),
            'props': props
        }


class BaseModelSerializer(RequestSerializerMixin, EntitySerializer):
    def before_create(self, validated_data):
        pass

    def after_create(self, instance, validated_data):
        pass

    @atomic()
    def create(self, validated_data):
        self.before_create(validated_data)

        instance = super().create(validated_data)

        self.after_create(instance, validated_data)

        return instance

    def before_update(self, instance, validated_data):
        pass

    def after_update(self, instance, validated_data):
        pass

    @atomic()
    def update(self, instance, validated_data):
        self.before_update(instance, validated_data)

        instance = super().update(instance, validated_data)

        self.after_update(instance, validated_data)

        return instance


class DynamicFieldsSerializerMixin:
    """
    Serializer mixin which allows pass to kwarg fields variable to return only specific fields
    """

    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        exclude_fields = kwargs.pop('exclude_fields', None)

        super().__init__(*args, **kwargs)

        existing = set(self.fields.keys())
        if exclude_fields:
            excluded = set(exclude_fields)
            for field_name in existing:
                if field_name in excluded:
                    self.fields.pop(field_name)
        elif fields:
            allowed = set(fields)
            for field_name in existing - allowed:
                if field_name in self.fields:
                    self.fields.pop(field_name)


class BulkSerializerMixin:
    def to_internal_value(self, data):
        ret = super(BulkSerializerMixin, self).to_internal_value(data)

        id_attr = getattr(self.Meta, 'update_lookup_field', 'id')
        request_method = getattr(self.context.get('request'), 'method', '')

        # add update_lookup_field field back to validated data
        # since super by default strips out read-only fields
        # hence id will no longer be present in validated_data
        if all((
                isinstance(self.root, serializers.ListSerializer),
                id_attr,
                request_method in ('PUT', 'PATCH')
        )):
            id_field = self.fields[id_attr]
            id_value = id_field.get_value(data)

            ret[id_attr] = id_value
        return ret


class BulkListSerializer(serializers.ListSerializer):
    update_lookup_field = 'id'
    delete_lookup_key = '_delete'

    def update(self, queryset, all_validated_data):
        id_attr = getattr(
            self.child.Meta,
            'update_lookup_field',
            self.update_lookup_field
        )

        all_validated_data_by_id = {
            i.pop(id_attr): i
            for i in all_validated_data
        }

        if not all((
                bool(i) and not inspect.isclass(i)
                for i in all_validated_data_by_id.keys()
        )):
            raise serializers.ValidationError(
                'Invalid id\'s to perform update action.'
            )

        # since this method is given a queryset which can have many
        # model instances, first find all objects to update
        # and only then update the models
        objects_to_update = queryset.filter(**{
            f'{id_attr}__in': all_validated_data_by_id.keys(),
        })

        if len(all_validated_data_by_id) != objects_to_update.count():
            raise serializers.ValidationError(
                'Could not find all objects to update.'
            )

        updated_objects = []

        for obj in objects_to_update:
            obj_id = getattr(obj, id_attr)
            obj_validated_data = all_validated_data_by_id.get(obj_id)
            is_to_delete = obj_validated_data.pop(self.delete_lookup_key, False)

            if is_to_delete:
                obj.delete()
            else:
                # use model serializer to actually update the model
                # in case that method is overwritten
                updated_objects.append(self.child.update(obj, obj_validated_data))

        return updated_objects


class ContentTypeSerializer(serializers.Serializer):
    type = ContentTypeNaturalKeyField(write_only=True)
    id = serializers.IntegerField(write_only=True)

    class Meta:
        fields = [
            'type',
            'id',
        ]

    def __init__(self, *args, **kwargs):
        self.natural_keys = kwargs.pop('natural_keys', [])
        super().__init__(*args, **kwargs)

    def validate(self, attrs):
        data = super().validate(attrs)

        ct: 'ContentType' = data['type']

        if self.natural_keys and ct.natural_key() not in self.natural_keys:
            raise serializers.ValidationError({'type': _('Not allowed type')})

        try:
            content_object = ct.get_object_for_this_type(pk=data['id'])
        except ObjectDoesNotExist:
            raise serializers.ValidationError({'id': _(
                'Invalid object id.'
            )})
        else:
            data['content_object'] = content_object
 
        return data