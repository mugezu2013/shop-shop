from django.conf import settings
from django.utils.translation import activate
from rest_framework import serializers

__all__ = (
    'AcceptLanguageMixin',
    'TypedModelSerializerMixin'
)

from shared.utils.base import get_content_type


class AcceptLanguageMixin:
    """
    Activate language, which was passed through 'Accept-Language' header
    """
    def dispatch(self, request, *args, **kwargs):
        if settings.USE_I18N:
            accept_lang = request.META.get('HTTP_ACCEPT_LANGUAGE')
            lang_codes = [lang[0] for lang in settings.LANGUAGES]
            if accept_lang in lang_codes:
                activate(accept_lang)
        return super().dispatch(request, *args, **kwargs)


class TypedModelSerializerMixin:
    """
    Adding a field `_type` which shows provided object's type.

    By default it's a ContentType natural key.
    """

    def __init__(self, *args, **kwargs):
        self.is_typed = kwargs.pop('is_typed', False)

        super(TypedModelSerializerMixin, self).__init__(*args, **kwargs)

    def get_fields(self):
        fields = super(TypedModelSerializerMixin, self).get_fields()

        if self.is_typed:
            fields['_type'] = serializers.SerializerMethodField(read_only=True)

        return fields

    def get__type(self, obj):
        return '.'.join(get_content_type(obj).natural_key())
