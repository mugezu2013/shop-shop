from django.db import models
from django.db.models import Max
from django.utils.translation import ugettext_lazy as _

__all__ = (
    'SortableModel',
)


class SortableModel(models.Model):
    """
    Abstract model.
    To inherit Meta options, like ordering, manually inherit Meta in your class

    Attrs:
        sort_order (PositiveIntegerField): order field. Used in sorting
    """
    auto_ordering = True

    sort_order = models.PositiveIntegerField(
        verbose_name=_('Sort order'),
        # editable=not auto_ordering,
        db_index=True,
        null=True,
        default=1
    )

    class Meta:
        abstract = True
        ordering = ['sort_order']

    def get_ordering_queryset(self):
        return self.__class__.objects.all()

    def get_max_sort_order(self, qs):
        existing_max = qs.aggregate(Max("sort_order"))
        existing_max = existing_max.get("sort_order__max")
        return existing_max

    def save(self, *args, **kwargs) -> None:
        if self.auto_ordering:
            if self.pk is None:
                qs = self.get_ordering_queryset()
                existing_max = self.get_max_sort_order(qs)
                self.sort_order = (
                    0 if existing_max is None else existing_max + 1
                )
        elif not self.sort_order:
            self.sort_order = 0
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs) -> None:
        if self.auto_ordering:
            qs = self.get_ordering_queryset()
            qs.filter(sort_order__gt=self.sort_order).update(
                sort_order=models.F('sort_order') - 1
            )
        super().delete(*args, **kwargs)
