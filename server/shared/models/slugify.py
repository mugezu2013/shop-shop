from django.db import models
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from unidecode import unidecode

__all__ = (
    'SlugifyMixin',
)


def unique_slug_generator(instance, slug, qs=None) -> str:
    if qs is None:
        qs = instance.__class__.objects.all()
    if qs.exclude(pk=instance.pk).filter(slug=slug).exists():
        key = get_random_string(length=8)
        slug = f'{slug}-{key}'
        return unique_slug_generator(instance, slug)
    return slug


class SlugifyMixin(models.Model):
    """
    Slugify mixin.
    Mixin to add slug field to models
    """

    SLUGABLE_FIELD = 'title'

    slug = models.SlugField(
        _('Slug'),
        db_index=True,
        max_length=256,
        blank=True, null=False,
    )

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.set_slug(slug=self.slug)

        return super().save(*args, **kwargs)

    def get_slug_qs(self):
        return self.__class__.objects.all()

    def get_slugable_value(self):
        return getattr(self, self.SLUGABLE_FIELD, '') or ''

    def set_slug(self, slug=None):
        """
        Create new slug on create or when still no slug.
        """
        if not slug:
            slug = slugify(unidecode(self.get_slugable_value()).lower())
        slug = unique_slug_generator(self, slug, self.get_slug_qs())
        setattr(self, 'slug', slug)
