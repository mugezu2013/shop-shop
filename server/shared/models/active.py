from django.db import models
from django.utils.translation import ugettext_lazy as _

__all__ = (
    'ActiveQuerySet',
    'ActiveMixin'
)


class ActiveQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def inactive(self):
        return self.filter(is_active=False)


class ActiveMixin(models.Model):
    is_active = models.BooleanField(
        _('Is active'),
        default=True,
        db_index=True,
    )

    objects = ActiveQuerySet.as_manager()

    class Meta:
        abstract = True

    def activate(self):
        if not self.is_active:
            self.is_active = True
            self.save(
                update_fields=["is_active", "updated"] if self.pk else None)

    def deactivate(self):
        if self.is_active:
            self.is_active = False
            self.save(
                update_fields=["is_active", "updated"] if self.pk else None)
