from django.db import models
from django.contrib.postgres.indexes import BrinIndex
from django.utils.translation import ugettext_lazy as _

__all__ = (
    'TimestampsMixin',
)


class TimestampsMixin(models.Model):
    """
    Timestamps abstract model

    Attrs:
        created_at (DateTimeField): created_at timestamp
        updated_at (DateTimeField): updated_at timestamp
    """
    created_at = models.DateTimeField(
        verbose_name=_('Created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=_('Updated at'),
        auto_now=True
    )

    class Meta:
        abstract = True
        ordering = ['-created_at']
        indexes = (
            BrinIndex(fields=['created_at']),
        )

