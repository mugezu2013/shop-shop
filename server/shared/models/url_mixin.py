from django.conf import settings
from django.contrib.sites.models import Site


# todo очень сирое, нужно теститить, дорабатывать
class UrlMixin(object):

    def get_url(self):
        try:
            path = self.get_url_path()
        except NotImplemented:
            raise
        protocol = getattr(settings, "PROTOCOL", "http")
        domain = Site.objects.get_current().domain
        port = getattr(settings, "PORT", "")
        if port:
            assert port.startswith(
                ":"), "The PORT setting must have a preceeding ':'."
        return "%s://%s%s%s" % (protocol, domain, port, path)

    def get_url_path(self):
        return None
