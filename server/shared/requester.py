from typing import Dict, Optional, Union

import requests


class Requester:
    @staticmethod
    def make_request(
        url: str,
        method: str = 'GET',
        data: Optional[Union[Dict, str]] = None,
        headers: Optional[Dict] = None,
        raise_for_status: bool = True
    ) -> Dict:
        """
        Makes an actual request on a given url. May be extended in
        future for additional parameters

        Args:
            url: URL to send a request.
            method: HTTP method to call.
            data: Post data.
            headers: Request headers.
            raise_for_status: Whether to raise HttpError on invalid response.
                Is True by default.

        Response:
            Dict: Response data as a python dictionary.

        Raises:
            HttpError: Request failed or response is invalid.
        """

        response = requests.request(
            method=method,
            url=url,
            data=data or {},
            params=data or {},
            headers=headers or {}
        )

        if raise_for_status:
            response.raise_for_status()

        response_json = response.json()

        return response_json
