import copy
import json
import os
import random
import re
import string
import time
from contextlib import contextmanager
from decimal import Decimal
from json import JSONEncoder
from urllib.parse import parse_qsl, urlparse
from uuid import UUID

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.utils.encoding import force_text
from django.utils.text import slugify as django_slugify
from django.utils.timezone import now
# from django.utils.lru_cache import lru_cache
from django.utils.translation.trans_real import language_code_prefix_re
from inflection import underscore
from unidecode import unidecode

__all__ = (
    'changed_get_params',
    'get_filename',
    'slugify',
    'random_string_generator',
    'unique_slug_generator',
    'clone_object',
    'get_credit_card_issuer',
    'get_domain',
    'get_object_or_none',
    'first_or_create',
    'serialize_decimal',
    'get_content_type',
    'remove_lang_from_path',
    'undo_snake_case_key',
    'duplicate',
    'partition',
    'all_fields',
    'choices_renderer',
    'to_json',
    'uuid_is_valid',
    'measure_time',
)


def changed_get_params(request):
    """
    Return get params, which was changed
    """
    prev_url = request.META.get('HTTP_REFERER')
    prev_url_parameters = parse_qsl(urlparse(prev_url)[4])
    curr_url = ''.join([request.path, '?', request.META.get('QUERY_STRING')])
    curr_url_parameters = parse_qsl(urlparse(curr_url)[4])
    if len(prev_url_parameters) < len(curr_url_parameters):
        result = curr_url_parameters[-1]
    else:
        result = [x[0] for x, y in zip(prev_url_parameters, curr_url_parameters)
                  if x[1] != y[1]]
    return result


def get_filename(path):
    """
    Return file's name
    """
    return os.path.basename(path)


def slugify(s):
    """
    Slugify string
    """
    return django_slugify(unidecode(s).lower())


def random_string_generator(size=10,
                            chars=string.ascii_lowercase + string.digits):
    """
    Return random string
    """
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, slugable_field_name, slug_field_name):
    """
    Takes a model instance, sluggable field name (such as 'title') of that
    model as string, slug field name (such as 'slug') of the model as string;
    returns a unique slug as string.
    Usage:
        def save(self, *args, **kwargs):
            if not self.slug:
                self.slug = unique_slug_generator(self, 'title', 'slug')
            super().save()
    """
    slug = slugify(getattr(instance, slugable_field_name))

    Klass = instance.__class__

    while Klass._default_manager.filter(**{slug_field_name: slug}).exists():
        slug = f"{slug}-{random_string_generator(size=4)}"
    return slug


def clone_object(instance):
    """
    Clone given object
    """
    Klass = instance.__class__
    clone = Klass(**{
        field.name: getattr(instance, field.name)
        for field in Klass._meta.get_fields()
    })
    clone.id = None
    clone.save()
    return clone


CREDIT_CARD_TYPES = [
    (r'^4[0-9]{12}(?:[0-9]{3})?$', 'visa', 'VISA'),
    (r'^5[1-5][0-9]{14}$', 'mastercard', 'MasterCard'),
    (r'^6(?:011|5[0-9]{2})[0-9]{12}$', 'discover', 'Discover'),
    (r'^3[47][0-9]{13}$', 'amex', 'American Express'),
    (r'^(?:(?:2131|1800|35\d{3})\d{11})$', 'jcb', 'JCB'),
    (r'^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$', 'diners', 'Diners Club'),
    (r'^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$', 'maestro', 'Maestro')]


def get_credit_card_issuer(number):
    """
    Return credit card type and name by given number
    """
    for regexp, card_type, name in CREDIT_CARD_TYPES:
        if re.match(regexp, number):
            return card_type, name
    return None, None


def get_domain():
    """
    Return domain
    """
    current_site = Site.objects.get_current()
    return current_site.domain


def get_object_or_none(klass, *args, **kwargs):
    """
    Return object or None
    """
    try:
        return klass._default_manager.get(*args, **kwargs)
    except klass.DoesNotExist:
        return None


def first_or_create(klass, **kwargs):
    """
    Return first object or create new one
    """
    return (
            klass._default_manager.filter(**kwargs).first() or
            klass._default_manager.create(**kwargs)
    )


def serialize_decimal(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    return JSONEncoder().default(obj)


# @lru_cache()
def get_content_type(cls):
    return ContentType.objects.get_for_model(cls)


def remove_lang_from_path(path: str, languages: list = None):
    regex_match = language_code_prefix_re.match(path)

    if regex_match:
        lang_code = regex_match.group(1)
        if languages is None:
            languages = [
                language_tuple[0] for
                language_tuple in settings.LANGUAGES
            ]
        if lang_code in languages:
            path = path[1 + len(lang_code):]
            if not path.startswith('/'):
                path = '/' + path

    return path


def undo_snake_case_key(key: str) -> str:
    assert isinstance(key, str)
    new_key = key.split('_')
    return new_key[0] + ''.join(
        [item[0].upper() + item[1:] for item in new_key[1:]])


def camel_case_to_snake_case(s: str):
    # s = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', s)
    # return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s).lower()
    return underscore(s)


def duplicate(obj, changes=None):
    """ Duplicates any object including m2m fields
    changes: any changes that should occur, example
    changes = (('fullname','name (copy)'), ('do_not_copy_me', ''))"""
    if not obj.pk:
        raise ValueError('Instance must be saved before it can be cloned.')
    duplicate = copy.copy(obj)
    duplicate.pk = None

    for change in changes:
        duplicate.__setattr__(change[0], change[1])

    duplicate.save()

    # trick to copy ManyToMany relations.
    for field in obj._meta.many_to_many:
        source = getattr(obj, field.attname)
        destination = getattr(duplicate, field.attname)
        for item in source.all():
            try:  # m2m, through fields will fail.
                destination.add(item)
            except:
                pass

    return


def get_object_by_type_and_id(type_: str, id_: str):
    content_type = ContentType.objects.filter(model=type_).first()
    if content_type:
        try:
            obj = content_type.get_object_for_this_type(pk=id_)
        except ObjectDoesNotExist:
            return None
        return obj
    return None


def partition(predicate, values):
    """
    Split the values into two sets, based on the return value of the function
    (True/False). e.g.:

        >>> partition(lambda x: x > 3, range(5))
        [0, 1, 2, 3], [4]
    """
    results = ([], [])
    for item in values:
        results[predicate(item)].append(item)
    return results


def generate_obj_number(prefix: str, obj):
    for i in range(1, 10000):
        number = f"{prefix}-{now().strftime('%d%m%y')}-{i:03}"
        if obj.__class__.objects.filter(number=number).exists():
            continue
        return number


def all_fields(cls, *exclude_fields):
    """
    Для админки Django, дабы избежать муторного перечисления
    всех полей, которые мы хотим вывести:
    list_display = ('any_field', 'any_field2',
        'any_field2', 'any_field3', 'any_field4', any_field5')
    Первый пример использования
    (этот пример выведет все поля модели MyModel):
    from django.contrib import admin
    from .models import MyModel
    from services.admin import all_fields
    class MyModelAdmin(admin.ModelAdmin):
        list_display = all_fields(MyModel)
    Второй пример использования
    (этот пример выведет все поля модели MyModel,
    кроме 'id' и 'date'):
    from django.contrib import admin
    from .models import MyModel
    from services.admin import all_fields
    class MyModelAdmin(admin.ModelAdmin):
        list_display = all_fields(MyModel, 'id', 'date')
    """
    return [field.name for field in cls._meta.fields if
            field.name not in exclude_fields]


def choices_renderer(choices, value_field: str = 'value',
                     display_field: str = 'display'):
    if hasattr(choices, '_display_map'):
        choices = choices._display_map.items()
    else:
        choices = choices.items()

    return [
        {
            value_field: choice_value,
            display_field: force_text(choice_name, strings_only=True)
        }
        for choice_value, choice_name in choices
    ]


def to_json(data: dict):
    return json.loads(json.dumps(data))


def uuid_is_valid(token):
    """
    Validate token.
    """
    if token is None:
        return False
    if isinstance(token, UUID):
        return True
    try:
        UUID(token)
    except ValueError:
        return False
    return True


@contextmanager
def measure_time(name: str):
    start_time = time.time()
    yield
    print(f"{name} --- %s seconds ---" % (time.time() - start_time))


def string_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

