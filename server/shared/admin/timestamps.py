__all__ = (
    'TimestampsAdmin',
)


class TimestampsAdmin:
    """
    Mixin.
    Sets created_at and updated_at timestamps as readonly fields
    """
    timestamps = ['created_at', 'updated_at']

    def get_readonly_fields(self, *args, **kwargs):
        """
        Add to list of readonly fields date created and date last updated model instance.

        Returns:
            tuple: List of model readonly fields with date created and date published.
        """
        fields = super().get_readonly_fields(*args, **kwargs)
        return tuple(fields) + tuple(self.timestamps)
